# SOFTWARE LICENCE

Copyright(c) 2020, Helmholtz-Zentrum fuer Umweltforschung GmbH - UFZ. 
All rights reserved.

The code in this repository is a property of:

Helmholtz-Zentrum fuer Umweltforschung GmbH - UFZ
Registered Office: Leipzig
Registration Office: Amtsgericht Leipzig
Trade Register: Nr. B 4703
Chairman of the Supervisory Board: MinDirig'in Oda Keppler
Scientific Director: Prof. Dr. Dr. h.c. Georg Teutsch
Administrative Director: Dr. Sabine König

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
