pf2_radialmodel <- create_ogs5(sim_name = "pf2_radialmodel",
					sim_path = "ogs5_benchmarks/ECLIPSE_DUMUX/2phase_flow_radialmodel",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
# pf2_radialmodel <- input_add_bc_bloc(pf2_radialmodel, bc_name = "BOUNDARY_CONDITION1",
#
# )
# gli ----------------------------------------------------------------------
pf2_radialmodel <- input_add_blocs_from_file(pf2_radialmodel,
                    sim_basename = "pf2_radialmodel",
					filename = "2pf_radialmodel.gli",
					file_dir = "ogs5_benchmarks/ECLIPSE_DUMUX/2phase_flow_radialmodel")

# ic ----------------------------------------------------------------------
# mfp ----------------------------------------------------------------------
pf2_radialmodel <- input_add_mfp_bloc(pf2_radialmodel, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "18 0",
					VISCOSITY = "1 5.1e-4"
)
pf2_radialmodel <- input_add_mfp_bloc(pf2_radialmodel, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE2",
					DENSITY = "18 0",
					VISCOSITY = "1 5.5e-5"
)
# mmp ----------------------------------------------------------------------
pf2_radialmodel <- input_add_mmp_bloc(pf2_radialmodel, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					POROSITY = "1 0.2",
					TORTUOSITY = "1 1.0",
					STORAGE = "1 0.000",
					PERMEABILITY_TENSOR = "ORTHOTROPIC 2.000000e-13 2.000000e-13 2.000000e-14",
					PERMEABILITY_SATURATION = "6 0.25 0.975 2.0 66 0.025 0.75 2.0 1e-9",
					CAPILLARY_PRESSURE = "6 5000"
)
# msh ----------------------------------------------------------------------
pf2_radialmodel <- input_add_blocs_from_file(pf2_radialmodel,
                    sim_basename = "pf2_radialmodel",
					filename = "2pf_radialmodel.msh",
					file_dir = "ogs5_benchmarks/ECLIPSE_DUMUX/2phase_flow_radialmodel")

# msp ----------------------------------------------------------------------
pf2_radialmodel <- input_add_msp_bloc(pf2_radialmodel, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.65000e+003"
)
# num ----------------------------------------------------------------------
pf2_radialmodel <- input_add_num_bloc(pf2_radialmodel, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "2 6 1.e-10 2000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-5 25 1.0"
)
# out ----------------------------------------------------------------------
pf2_radialmodel <- input_add_out_bloc(pf2_radialmodel, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 PRESSURE2 SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1 VELOCITY_X2 VELOCITY_Y2 VELOCITY_Z2",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
pf2_radialmodel <- input_add_pcs_bloc(pf2_radialmodel, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NUM_TYPE = "NEW",
					USE_PRECALCULATED_FILES = "",
					SAVE_ECLIPSE_DATA_FILES = "",
					SIMULATOR = "ECLIPSE",
					SIMULATOR_PATH = "C:\\ecl\\2012.1\\bin\\pc_x86_64\\eclipse.exe",
					SIMULATOR_MODEL_PATH = "./eclipse/ECL.DATA",
					ELEMENT_MATRIX_OUTPUT = "0",
					ST_RHS = "1",
					BOUNDARY_CONDITION_OUTPUT = ""
)
# st ----------------------------------------------------------------------
pf2_radialmodel <- input_add_st_bloc(pf2_radialmodel, st_name = "SOURCE_TERM1",

)
# tim ----------------------------------------------------------------------
pf2_radialmodel <- input_add_tim_bloc(pf2_radialmodel, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					TIME_STEPS = "10 86400.0",
					TIME_END = "4320000",
					TIME_START = "0.0"
)
