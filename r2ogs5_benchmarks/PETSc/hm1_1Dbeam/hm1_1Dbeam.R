hm1_1Dbeam <- create_ogs5(sim_name = "hm1_1Dbeam",
					sim_path = "r2ogs5_benchmarks/tmp/PETSc/hm1_1Dbeam",
					sim_id = 1L)

# msh ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
					sim_basename = "hm1_1Dbeam",
					filename = "hm1_1Dbeam_partitioned_cfg2.msh",
					file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# msh ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
					sim_basename = "hm1_1Dbeam",
					filename = "hm1_1Dbeam_partitioned_elems_2.msh",
					file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# msh ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
					sim_basename = "hm1_1Dbeam",
					filename = "hm1_1Dbeam_partitioned_nodes_2.msh",
					file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# bc ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE1",
					DIS_TYPE = "LINEAR 4 0 0.00000E+00 1 0.00000E+00 2 0.00000E+00 3 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE2",
					DIS_TYPE = "LINEAR 4 4 0.00000E+00 5 0.00000E+00 6 0.00000E+00 7 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE3",
					DIS_TYPE = "LINEAR 4 8 0.00000E+00 9 0.00000E+00 10 0.00000E+00 11 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE4",
					DIS_TYPE = "LINEAR 4 12 0.00000E+00 13 0.00000E+00 14 0.00000E+00 15 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE5",
					DIS_TYPE = "LINEAR 4 16 0.00000E+00 17 0.00000E+00 18 0.00000E+00 19 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "SURFACE SURFACE6",
					DIS_TYPE = "LINEAR 4 20 0.00000E+00 21 0.00000E+00 22 0.00000E+00 23 0.00000E+00",
					TIM_TYPE = "CURVE 1"
)
hm1_1Dbeam <- input_add_bc_bloc(hm1_1Dbeam, bc_name = "BOUNDARY_CONDITION7",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "SURFACE SURFACE7",
					DIS_TYPE = "LINEAR 4 24 1.00000E+06 25 1.00000E+06 26 1.00000E+06 27 1.00000E+06",
					TIM_TYPE = "CURVE 1"
)
# gli ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
					sim_basename = "hm1_1Dbeam",
					filename = "hm1_1Dbeam.gli",
					file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# ic ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_ic_bloc(hm1_1Dbeam, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.5E+6"
)
# mesh ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
                    sim_basename = "",
                    filename = "hm1_1Dbeam.mesh",
                    file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# mfp ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_mfp_bloc(hm1_1Dbeam, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.0E+0",
					VISCOSITY = "1 1.0E-3"
)
# mmp ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_mmp_bloc(hm1_1Dbeam, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					POROSITY = "1 0.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0E-12"
)
# msh ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_blocs_from_file(hm1_1Dbeam,
					sim_basename = "hm1_1Dbeam",
					filename = "hm1_1Dbeam.msh",
					file_dir = "ogs5_benchmarks/PETSc/hm1_1Dbeam")

# msp ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_msp_bloc(hm1_1Dbeam, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0E+0",
					ELASTICITY = "POISSION 0.25\n YOUNGS_MODULUS\n 1 2.5E+10",
					BIOT_CONSTANT = "1.0"
)
# num ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_num_bloc(hm1_1Dbeam, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					LINEAR_SOLVER = "petsc bcgs bjacobi 1.e-20 10000"
)
hm1_1Dbeam <- input_add_num_bloc(hm1_1Dbeam, num_name = "NUMERICS2",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "petsc bcgs bjacobi 1.e-10 10000"
)
# out ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_out_bloc(hm1_1Dbeam, out_name = "OUTPUT1",
					PCS_TYPE = "LIQUID_FLOW",
					NOD_VALUES = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1"
)
hm1_1Dbeam <- input_add_out_bloc(hm1_1Dbeam, out_name = "OUTPUT2",
					PCS_TYPE = "DEFORMATION",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1"
)
# pcs ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_pcs_bloc(hm1_1Dbeam, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
hm1_1Dbeam <- input_add_pcs_bloc(hm1_1Dbeam, pcs_name = "PROCESS2",
					PCS_TYPE = "DEFORMATION",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_rfd_bloc(hm1_1Dbeam, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(-1,
										2),
								value = c(1,
										1))
)
# tim ----------------------------------------------------------------------
hm1_1Dbeam <- input_add_tim_bloc(hm1_1Dbeam, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "2 1.0",
					TIME_END = "1.0",
					TIME_START = "0.0"
)
hm1_1Dbeam <- input_add_tim_bloc(hm1_1Dbeam, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "DEFORMATION",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "2 1.0",
					TIME_END = "1.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(hm1_1Dbeam,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(hm1_1Dbeam, ogs_exe = "ogs_petsc-gems",
					run_path = NULL, #"ogs5_benchmarks/PETSc/hm1_1Dbeam",
					log_output = TRUE,
					use_mpi = TRUE,
					number_of_cores = 2,
					log_path = "r2ogs5_benchmarks/tmp/PETSc/hm1_1Dbeam/log")
