richards <- create_ogs5(sim_name = "h_us_quad",
					sim_path = "r2ogs5_benchmarks/tmp/PETSc/Richards",
					sim_id = 1L)

# msh ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad_partitioned_cfg4.msh",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# msh ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad_partitioned_elems_4.msh",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# msh ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad_partitioned_nodes_4.msh",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# msh ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad_partitioned.msh",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# bc ----------------------------------------------------------------------
richards <- input_add_bc_bloc(richards, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT -21500."
)
richards <- input_add_bc_bloc(richards, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 0.0"
)
# gli ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad.gli",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# ic ----------------------------------------------------------------------
richards <- input_add_ic_bloc(richards, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT -21500"
)
# mfp ----------------------------------------------------------------------
richards <- input_add_mfp_bloc(richards, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
richards <- input_add_mmp_bloc(richards, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.38",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.46e-13",
					PERMEABILITY_SATURATION = "0 2 0 2",
					CAPILLARY_PRESSURE = "0 1"
)
# msh ----------------------------------------------------------------------
richards <- input_add_blocs_from_file(richards,
					sim_basename = "h_us_quad",
					filename = "h_us_quad.msh",
					file_dir = "ogs5_benchmarks/PETSc/Richards")

# num ----------------------------------------------------------------------
richards <- input_add_num_bloc(richards, num_name = "NUMERICS1",
					PCS_TYPE = "PRESSURE1",
					ELE_UPWINDING = "0.5",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "petsc bcgs bjacobi 1.e-7 2000 1.0",
					NON_LINEAR_SOLVER = "PICARD 1.0e-3 50 0.0"
)
# out ----------------------------------------------------------------------
richards <- input_add_out_bloc(richards, out_name = "OUTPUT1",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "DOMAIN",
					TIM_TYPE = "0.01 0.1 1.0 180. 1800. 3600. 7200. 32400. 61200.",
					DAT_TYPE = "TECPLOT"
)
# pcs ----------------------------------------------------------------------
richards <- input_add_pcs_bloc(richards, pcs_name = "PROCESS1",
					PCS_TYPE = "RICHARDS_FLOW",
					NUM_TYPE = "NEW"
)
# rfd ----------------------------------------------------------------------
richards <- input_add_rfd_bloc(richards, rfd_name = "CURVES2",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.26315789,
										0.39473684,
										0.52631579,
										0.57894737,
										0.63157895,
										0.65789474,
										0.78947368,
										0.81578947,
										0.84210526,
										0.86842105,
										0.89473684,
										0.92105263,
										0.94894737,
										0.96052632,
										0.97368421,
										0.98684211,
										1.00000000),
								value = c(44627.5611,
										26399.6732,
										15616.8683,
										12658.7874,
										10261.0136,
										9238.24221,
										5464.93174,
										4920.21206,
										4429.78757,
										3988.24638,
										3590.71602,
										3232.80969,
										2892.29884,
										2462.21599,
										2050.45507,
										1707.55369,
										1421.99634))
)
richards <- input_add_rfd_bloc(richards, rfd_name = "CURVES3",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.26315789,
										0.39473684,
										0.52631579,
										0.57894737,
										0.63157895,
										0.65789474,
										0.78947368,
										0.81578947,
										0.84210526,
										0.86842105,
										0.89473684,
										0.92105263,
										0.94894737,
										0.96052632,
										0.97368421,
										0.98684211,
										1.00000000),
								value = c(4.4323E-05,
										0.00026547,
										0.00159003,
										0.00325358,
										0.00665757,
										0.00952343,
										0.05704014,
										0.08159396,
										0.11671736,
										0.16696017,
										0.23883078,
										0.34163922,
										0.49931406,
										0.58449912,
										0.69907308,
										0.8361059,
										1.0000000))
)
# st ----------------------------------------------------------------------
richards <- input_add_st_bloc(richards, st_name = "SOURCE_TERM1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
# tim ----------------------------------------------------------------------
richards <- input_add_tim_bloc(richards, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "RICHARDS_FLOW",
					TIME_STEPS = "100 10 1000 100",
					TIME_END = "61200.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(richards,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(richards, ogs_exe = "ogs_petsc-gems",
					run_path = NULL,
					log_output = TRUE,
					use_mpi = TRUE,
					number_of_cores = 4,
					log_path = "r2ogs5_benchmarks/tmp/PETSc/Richards/log")
