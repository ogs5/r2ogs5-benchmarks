h_tri <- create_ogs5(sim_name = "h_tri",
					sim_path = "r2ogs5_benchmarks/tmp/PETSc/h_tri",
					sim_id = 1L)

# msh ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri_partitioned_cfg3.msh",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# msh ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri_partitioned_elems_3.msh",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# msh ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri_partitioned_nodes_3.msh",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# msh ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri_partitioned.msh",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# bc ----------------------------------------------------------------------
h_tri <- input_add_bc_bloc(h_tri, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0.000000e+004"
)
h_tri <- input_add_bc_bloc(h_tri, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 2.000000e+004"
)
# gli ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri.gli",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# ic ----------------------------------------------------------------------
h_tri <- input_add_ic_bloc(h_tri, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
h_tri <- input_add_mfp_bloc(h_tri, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.0",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
h_tri <- input_add_mmp_bloc(h_tri, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					GEO_TYPE = "SURFACE SURFACE0",
					POROSITY = "1 2.000000e-001",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.000000e-12"
)
# msh ----------------------------------------------------------------------
h_tri <- input_add_blocs_from_file(h_tri,
					sim_basename = "h_tri",
					filename = "h_tri.msh",
					file_dir = "ogs5_benchmarks/PETSc/h_tri")

# num ----------------------------------------------------------------------
h_tri <- input_add_num_bloc(h_tri, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					LINEAR_SOLVER = "petsc cg jacobi 1.e-7 2000 1.0"
)
# out ----------------------------------------------------------------------
h_tri <- input_add_out_bloc(h_tri, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "8.640000e+004"
)
# pcs ----------------------------------------------------------------------
h_tri <- input_add_pcs_bloc(h_tri, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# tim ----------------------------------------------------------------------
h_tri <- input_add_tim_bloc(h_tri, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "1 8.640000e+004",
					TIME_END = "8.640000e+004",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(h_tri,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(h_tri, ogs_exe = "ogs_petsc-gems",
					run_path = NULL,
					log_output = TRUE,
					use_mpi = TRUE,
					number_of_cores = 3,
					log_path = "r2ogs5_benchmarks/tmp/PETSc/h_tri/log")
