lau <- create_ogs5(sim_name = "lau",
					sim_path = "r2ogs5_benchmarks/tmp/NUMERICS/DISCRETE_FEATURES/Lauwerier",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
lau <- input_add_bc_bloc(lau, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT 100000"
)
lau <- input_add_bc_bloc(lau, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1"
)
# gli ----------------------------------------------------------------------
lau <- input_add_blocs_from_file(lau,
					sim_basename = "Lauwerier",
					filename = "Lauwerier.gli",
					file_dir = "ogs5_benchmarks/NUMERICS/DISCRETE_FEATURES/Lauwerier/")

# ic ----------------------------------------------------------------------
lau <- input_add_ic_bloc(lau, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 100000"
)
lau <- input_add_ic_bloc(lau, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
lau <- input_add_ic_bloc(lau, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1"
)
# mfp ----------------------------------------------------------------------
lau <- input_add_mfp_bloc(lau, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1000",
					VISCOSITY = "1 0.001",
					SPECIFIC_HEAT_CAPACITY = "1 4000",
					HEAT_CONDUCTIVITY = "1 1.0E-8"
)
# mmp ----------------------------------------------------------------------
lau <- input_add_mmp_bloc(lau, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.0E+0",
					POROSITY = "1 0",
					STORAGE = "1 0.0",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 0",
					HEAT_DISPERSION = "1 0.000000e+000 0.000000e+000"
)
lau <- input_add_mmp_bloc(lau, NAME = "MEDIUM_PROPERTIES2",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.0E-3",
					POROSITY = "1 1",
					STORAGE = "1 0.0",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0e-15",
					HEAT_DISPERSION = "1 0.000000e+000 0.000000e+000"
)
# msh ----------------------------------------------------------------------
lau <- input_add_blocs_from_file(lau,
					sim_basename = "Lauwerier",
					filename = "Lauwerier.msh",
					file_dir = "ogs5_benchmarks/NUMERICS/DISCRETE_FEATURES/Lauwerier/")

# msp ----------------------------------------------------------------------
lau <- input_add_msp_bloc(lau, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2500",
					THERMAL = "EXPANSION\n 1.0e-5\n CAPACITY\n 1 1000\n CONDUCTIVITY\n 1 1\n CONDUCTIVITY_TENSOR\n ORTHOTROPIC 3 0 0 1"
)
lau <- input_add_msp_bloc(lau, NAME = "SOLID_PROPERTIES2",
					DENSITY = "1 2500",
					THERMAL = "EXPANSION\n 1.0e-5 CAPACITY\n 1 1000\n CONDUCTIVITY\n 1 1"
)
# num ----------------------------------------------------------------------
lau <- input_add_num_bloc(lau, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					LINEAR_SOLVER = "2 2 1.e-016 5000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-3 1 0.0"
)
lau <- input_add_num_bloc(lau, num_name = "NUMERICS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 0 1.e-012 1000 1.0 100 4",
					ELE_GAUSS_POINTS = "2",
					NON_LINEAR_SOLVER = "PICARD 1e-3 1 0.0"
)
# out ----------------------------------------------------------------------
lau <- input_add_out_bloc(lau, out_name = "OUTPUT1",
					NOD_VALUES = "TEMPERATURE1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1",
					GEO_TYPE = "POLYLINE V2",
					TIM_TYPE = "5.0E7 1.0E8 1.5E8 2.0E8 2.5E8 3.0E8 3.5E8 4.0E8 4.5E8 5.0E8"
)
lau <- input_add_out_bloc(lau, out_name = "OUTPUT2",
					NOD_VALUES = "TEMPERATURE1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1",
					GEO_TYPE = "POLYLINE FRACTURE",
					TIM_TYPE = "5.0E7 1.0E8 1.5E8 2.0E8 2.5E8 3.0E8 3.5E8 4.0E8 4.5E8 5.0E8"
)
# pcs ----------------------------------------------------------------------
lau <- input_add_pcs_bloc(lau, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					NUM_TYPE = "NEW",
					TIM_TYPE = "STEADY"
)
lau <- input_add_pcs_bloc(lau, pcs_name = "PROCESS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					NUM_TYPE = "NEW",
					MEMORY_TYPE = "1"
)
# st ----------------------------------------------------------------------
lau <- input_add_st_bloc(lau, st_name = "SOURCE_TERM1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1e-7"
)
# tim ----------------------------------------------------------------------
lau <- input_add_tim_bloc(lau, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "250 2E6",
					TIME_END = "1E99",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(lau,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(lau, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/NUMERICS/DISCRETE_FEATURES/Lauwerier/log")
