fem_fct <- create_ogs5(sim_name = "fem_fct",
					sim_path = "r2ogs5_benchmarks/tmp/NUMERICS/FEM_FCT",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
fem_fct <- input_add_bc_bloc(fem_fct, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 100000"
)
fem_fct <- input_add_bc_bloc(fem_fct, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "CONCENTRATION0",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0"
)
# gli ----------------------------------------------------------------------
fem_fct <- input_add_blocs_from_file(fem_fct,
					sim_basename = "mass_adv_line",
					filename = "mass_adv_line.gli",
					file_dir = "ogs5_benchmarks/NUMERICS/FEM_FCT/")

# ic ----------------------------------------------------------------------
fem_fct <- input_add_ic_bloc(fem_fct, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "CONCENTRATION0",
					GEO_TYPE = "POLYLINE PLY_1",
					DIS_TYPE = "CONSTANT 1"
)
# mcp ----------------------------------------------------------------------
fem_fct <- input_add_mcp_bloc(fem_fct, NAME = "CONCENTRATION0",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
# mfp ----------------------------------------------------------------------
fem_fct <- input_add_mfp_bloc(fem_fct, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 3.000000e-004",
					SPECIFIC_HEAT_CAPACITY = "1 4000",
					HEAT_CONDUCTIVITY = "1 0.6"
)
# mmp ----------------------------------------------------------------------
fem_fct <- input_add_mmp_bloc(fem_fct, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 1.0",
					STORAGE = "1 0.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 1e-15",
					TORTUOSITY = "1 1.000000e+000",
					MASS_DISPERSION = "1 0 0"
)
# msh ----------------------------------------------------------------------
fem_fct <- input_add_blocs_from_file(fem_fct,
					sim_basename = "mass_adv_line",
					filename = "mass_adv_line.msh",
					file_dir = "ogs5_benchmarks/NUMERICS/FEM_FCT/")

# msp ----------------------------------------------------------------------
fem_fct <- input_add_msp_bloc(fem_fct, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2650",
					THERMAL = "EXPANSION\n 1.0e-5\n CAPACITY\n 1 1000\n CONDUCTIVITY\n 1 3.0"
)
# num ----------------------------------------------------------------------
fem_fct <- input_add_num_bloc(fem_fct, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					LINEAR_SOLVER = "2 2 1.e-15 1000 1.0 100 4",
					ELE_GAUSS_POINTS = "2"
)
fem_fct <- input_add_num_bloc(fem_fct, num_name = "NUMERICS2",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 2 1.e-15 1000 1.0 100 4",
					FEM_FCT = "1 0"
)
# out ----------------------------------------------------------------------
fem_fct <- input_add_out_bloc(fem_fct, out_name = "OUTPUT1",
					NOD_VALUES = "CONCENTRATION0",
					GEO_TYPE = "POLYLINE PLY_0",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 10"
)
# pcs ----------------------------------------------------------------------
fem_fct <- input_add_pcs_bloc(fem_fct, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					TIM_TYPE = "STEADY"
)
fem_fct <- input_add_pcs_bloc(fem_fct, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					MEMORY_TYPE = "1"
)
# st ----------------------------------------------------------------------
fem_fct <- input_add_st_bloc(fem_fct, st_name = "SOURCE_TERM1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT_NEUMANN 1"
)
# tim ----------------------------------------------------------------------
fem_fct <- input_add_tim_bloc(fem_fct, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "100 1e-3",
					TIME_END = "0.5",
					TIME_START = "0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(fem_fct,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(fem_fct, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/NUMERICS/FEM_FCT/log")
