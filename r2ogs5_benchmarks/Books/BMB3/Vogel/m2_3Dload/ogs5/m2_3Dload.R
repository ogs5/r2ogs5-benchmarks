# Benchmark: m2_3Dload

# define ogs5 obj ---------------------------------------------------------
m2_3Dload <- create_ogs5(sim_name = "m2_3Dload",
					sim_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/m2_3Dload/ogs5",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
m2_3Dload <- input_add_bc_bloc(m2_3Dload, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE1",
					DIS_TYPE = "LINEAR 4 0 0.00000E+00 1 0.00000E+00 2 0.00000E+00 3 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_bc_bloc(m2_3Dload, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE2",
					DIS_TYPE = "LINEAR 4 4 0.00000E+00 5 0.00000E+00 6 0.00000E+00 7 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_bc_bloc(m2_3Dload, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE3",
					DIS_TYPE = "LINEAR 4 8 0.00000E+00 9 0.00000E+00 10 0.00000E+00 11 0.00000E+00",
					TIM_TYPE = "CURVE 1")

# gli ----------------------------------------------------------------------
m2_3Dload <- input_add_blocs_from_file(m2_3Dload,
                    sim_basename = "m2_3Dload",
					filename = "m2_3Dload.gli",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/m2_3Dload/ogs5")

# mmp ----------------------------------------------------------------------
m2_3Dload <- input_add_mmp_bloc(m2_3Dload, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3")

# msh ----------------------------------------------------------------------
m2_3Dload <- input_add_blocs_from_file(m2_3Dload,
                    sim_basename = "m2_3Dload",
					filename = "m2_3Dload.msh",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/m2_3Dload/ogs5")

# msp ----------------------------------------------------------------------
m2_3Dload <- input_add_msp_bloc(m2_3Dload, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0",
					ELASTICITY = "POISSION 0.25 YOUNGS_MODULUS 1 25000",
					STRESS_UNIT = "MegaPascal",
					# CREEP_BGRA = ! is not a valid skey !"0.18 5.0 54000.0"
					)

# num ----------------------------------------------------------------------
m2_3Dload <- input_add_num_bloc(m2_3Dload, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 1 1.0e-10 10000 1.0 101 4")

# out ----------------------------------------------------------------------
m2_3Dload <- input_add_out_bloc(m2_3Dload, out_name = "OUTPUT1",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1.0")

# pcs ----------------------------------------------------------------------
m2_3Dload <- input_add_pcs_bloc(m2_3Dload, pcs_name = "PROCESS1",
					PCS_TYPE = "DEFORMATION",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
m2_3Dload <- input_add_rfd_bloc(m2_3Dload, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(-1,
										2),
								value = c(1,
										1)))

# st ----------------------------------------------------------------------
m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE4",
					DIS_TYPE = "LINEAR_NEUMANN 4 12 4.00000E+01 13 4.00000E+01 14 4.00000E+01 15 4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE5",
					DIS_TYPE = "LINEAR_NEUMANN 4 16 0.00000E+00 17 0.00000E+00 18 0.00000E+00 19 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE6",
					DIS_TYPE = "LINEAR_NEUMANN 4 20 0.00000E+00 21 0.00000E+00 22 0.00000E+00 23 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM4",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE7",
					DIS_TYPE = "LINEAR_NEUMANN 4 24 -4.00000E+01 25 -4.00000E+01 26 -4.00000E+01 27 -4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM5",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE8",
					DIS_TYPE = "LINEAR_NEUMANN 4 28 0.00000E+00 29 0.00000E+00 30 0.00000E+00 31 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM6",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE9",
					DIS_TYPE = "LINEAR_NEUMANN 4 32 0.00000E+00 33 0.00000E+00 34 0.00000E+00 35 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM7",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE10",
					DIS_TYPE = "LINEAR_NEUMANN 4 36 0.00000E+00 37 0.00000E+00 38 0.00000E+00 39 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM8",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE11",
					DIS_TYPE = "LINEAR_NEUMANN 4 40 4.00000E+01 41 4.00000E+01 42 4.00000E+01 43 4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM9",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE12",
					DIS_TYPE = "LINEAR_NEUMANN 4 44 4.00000E+01 45 4.00000E+01 46 4.00000E+01 47 4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM10",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE13",
					DIS_TYPE = "LINEAR_NEUMANN 4 48 -4.00000E+01 49 -4.00000E+01 50 -4.00000E+01 51 -4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM11",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE14",
					DIS_TYPE = "LINEAR_NEUMANN 4 52 0.00000E+00 53 0.00000E+00 54 0.00000E+00 55 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM12",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE15",
					DIS_TYPE = "LINEAR_NEUMANN 4 56 0.00000E+00 57 0.00000E+00 58 0.00000E+00 59 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM13",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE16",
					DIS_TYPE = "LINEAR_NEUMANN 4 60 4.00000E+01 61 4.00000E+01 62 4.00000E+01 63 4.00000E+01",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM14",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE17",
					DIS_TYPE = "LINEAR_NEUMANN 4 64 0.00000E+00 65 0.00000E+00 66 0.00000E+00 67 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m2_3Dload <- input_add_st_bloc(m2_3Dload, st_name = "SOURCE_TERM15",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE18",
					DIS_TYPE = "LINEAR_NEUMANN 4 68 0.00000E+00 69 0.00000E+00 70 0.00000E+00 71 0.00000E+00",
					TIM_TYPE = "CURVE 1")

# tim ----------------------------------------------------------------------
m2_3Dload <- input_add_tim_bloc(m2_3Dload, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION",
					TIME_UNIT = "DAY",
					TIME_STEPS = "1 1.E-10 105 0.01",
					TIME_END = "1.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(m2_3Dload, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = m2_3Dload,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/m2_3Dload/ogs5/log")
