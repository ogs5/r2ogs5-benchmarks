# Benchmark: hm2_1Dcolumn1

# define ogs5 obj ---------------------------------------------------------
hm2_1Dcolumn1 <- create_ogs5(sim_name = "hm2_1Dcolumn1",
					sim_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/hm2_1Dcolumn1/ogs5",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE1",
					DIS_TYPE = "LINEAR 4 0 0.00000E+00 1 0.00000E+00 2 0.00000E+00 3 0.00000E+00",
					TIM_TYPE = "CURVE 1")

hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE2",
					DIS_TYPE = "LINEAR 4 4 0.00000E+00 5 0.00000E+00 6 0.00000E+00 7 0.00000E+00",
					TIM_TYPE = "CURVE 1")

hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE3",
					DIS_TYPE = "LINEAR 4 8 0.00000E+00 9 0.00000E+00 10 0.00000E+00 11 0.00000E+00",
					TIM_TYPE = "CURVE 1")

hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE4",
					DIS_TYPE = "LINEAR 4 12 0.00000E+00 13 0.00000E+00 14 0.00000E+00 15 0.00000E+00",
					TIM_TYPE = "CURVE 1")

hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE5",
					DIS_TYPE = "LINEAR 4 16 0.00000E+00 17 0.00000E+00 18 0.00000E+00 19 0.00000E+00",
					TIM_TYPE = "CURVE 1")

hm2_1Dcolumn1 <- input_add_bc_bloc(hm2_1Dcolumn1, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "SURFACE SURFACE6",
					DIS_TYPE = "LINEAR 4 20 0.00000E+00 21 0.00000E+00 22 0.00000E+00 23 0.00000E+00",
					TIM_TYPE = "CURVE 1")

# gli ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_blocs_from_file(hm2_1Dcolumn1,
                    sim_basename = "hm2_1Dcolumn1",
					filename = "hm2_1Dcolumn1.gli",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/hm2_1Dcolumn1/ogs5")

# ic ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_ic_bloc(hm2_1Dcolumn1, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0")

# mfp ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_mfp_bloc(hm2_1Dcolumn1, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.0E+0",
					VISCOSITY = "1 1.0E-3")

# mmp ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_mmp_bloc(hm2_1Dcolumn1, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					POROSITY = "1 0.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0E-10")

# msh ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_blocs_from_file(hm2_1Dcolumn1,
                    sim_basename = "hm2_1Dcolumn1",
					filename = "hm2_1Dcolumn1.msh",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/hm2_1Dcolumn1/ogs5")

# msp ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_msp_bloc(hm2_1Dcolumn1, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0E+0",
					ELASTICITY = "POISSION 0.2 YOUNGS_MODULUS 1 30000",
					BIOT_CONSTANT = "1.0")

# num ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_num_bloc(hm2_1Dcolumn1, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION_FLOW",
					LINEAR_SOLVER = "2 1 1.e-10 10000 1.0 100 4")

# out ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_out_bloc(hm2_1Dcolumn1, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "6 12")

# pcs ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_pcs_bloc(hm2_1Dcolumn1, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW DEFORMATION",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_rfd_bloc(hm2_1Dcolumn1, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(-1,
										0,
										13),
								value = c(0,
										0,
										13)))

# st ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_st_bloc(hm2_1Dcolumn1, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE7",
					DIS_TYPE = "LINEAR_NEUMANN 4 24 -1.00000E+03 25 -1.00000E+03 26 -1.00000E+03 27 -1.00000E+03",
					TIM_TYPE = "CURVE 1")

# tim ----------------------------------------------------------------------
hm2_1Dcolumn1 <- input_add_tim_bloc(hm2_1Dcolumn1, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION_FLOW",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "250 0.05",
					TIME_END = "12.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(hm2_1Dcolumn1, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = hm2_1Dcolumn1,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/hm2_1Dcolumn1/ogs5/log")
