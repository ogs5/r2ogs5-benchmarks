# Benchmark: tm1_3Dsquare

# define ogs5 obj ---------------------------------------------------------
tm1_3Dsquare <- create_ogs5(sim_name = "tm1_3Dsquare",
					sim_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/tm1_3Dsquare/ogs5",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_bc_bloc(tm1_3Dsquare, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE1",
					DIS_TYPE = "LINEAR 4 0 0.00000E+00 1 0.00000E+00 2 0.00000E+00 3 0.00000E+00",
					TIM_TYPE = "CURVE 1")

tm1_3Dsquare <- input_add_bc_bloc(tm1_3Dsquare, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE2",
					DIS_TYPE = "LINEAR 4 4 0.00000E+00 5 0.00000E+00 6 0.00000E+00 7 0.00000E+00",
					TIM_TYPE = "CURVE 1")

tm1_3Dsquare <- input_add_bc_bloc(tm1_3Dsquare, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE3",
					DIS_TYPE = "LINEAR 4 8 0.00000E+00 9 0.00000E+00 10 0.00000E+00 11 0.00000E+00",
					TIM_TYPE = "CURVE 1")

tm1_3Dsquare <- input_add_bc_bloc(tm1_3Dsquare, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "SURFACE SURFACE4",
					DIS_TYPE = "LINEAR 4 12 -1.00000E+01 13 -1.00000E+01 14 -1.00000E+01 15 -1.00000E+01",
					TIM_TYPE = "CURVE 1")

tm1_3Dsquare <- input_add_bc_bloc(tm1_3Dsquare, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "SURFACE SURFACE5",
					DIS_TYPE = "LINEAR 4 16 1.00000E+01 17 1.00000E+01 18 1.00000E+01 19 1.00000E+01",
					TIM_TYPE = "CURVE 1")

# gli ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_blocs_from_file(tm1_3Dsquare,
                    sim_basename = "tm1_3Dsquare",
					filename = "tm1_3Dsquare.gli",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/tm1_3Dsquare/ogs5")

# ic ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_ic_bloc(tm1_3Dsquare, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0E-10")

# mfp ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_mfp_bloc(tm1_3Dsquare, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 0.0E+0",
					VISCOSITY = "1 1.0E-3")

# mmp ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_mmp_bloc(tm1_3Dsquare, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3")

# msh ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_blocs_from_file(tm1_3Dsquare,
                    sim_basename = "tm1_3Dsquare",
					filename = "tm1_3Dsquare.msh",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/tm1_3Dsquare/ogs5")

# msp ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_msp_bloc(tm1_3Dsquare, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0",
					THERMAL = "EXPANSION 1.0E-3 CAPACITY 1 0.0 CONDUCTIVITY 1 1.0",
					ELASTICITY = "POISSION 0.2 YOUNGS_MODULUS 1 1.0E+10")

# num ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_num_bloc(tm1_3Dsquare, num_name = "NUMERICS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 1 1.0e-10 10000 1.0 101 4")

tm1_3Dsquare <- input_add_num_bloc(tm1_3Dsquare, num_name = "NUMERICS2",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 1 1.0e-10 10000 1.0 101 4")

# out ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_out_bloc(tm1_3Dsquare, out_name = "OUTPUT1",
					NOD_VALUES = "TEMPERATURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1")

# pcs ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_pcs_bloc(tm1_3Dsquare, pcs_name = "PROCESS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					NUM_TYPE = "NEW")

tm1_3Dsquare <- input_add_pcs_bloc(tm1_3Dsquare, pcs_name = "PROCESS2",
					PCS_TYPE = "DEFORMATION",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_rfd_bloc(tm1_3Dsquare, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(-1,
										2),
								value = c(1,
										1)))

# tim ----------------------------------------------------------------------
tm1_3Dsquare <- input_add_tim_bloc(tm1_3Dsquare, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "2 1.0",
					TIME_END = "1.0",
					TIME_START = "0.0")

tm1_3Dsquare <- input_add_tim_bloc(tm1_3Dsquare, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "DEFORMATION",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "2 1.0",
					TIME_END = "1.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(tm1_3Dsquare, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = tm1_3Dsquare,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/tm1_3Dsquare/ogs5/log")
