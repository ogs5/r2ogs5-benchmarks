# Benchmark: m1_3Dwaves

# define ogs5 obj ---------------------------------------------------------
m1_3Dwaves <- create_ogs5(sim_name = "m1_3Dwaves",
					sim_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
m1_3Dwaves <- input_add_bc_bloc(m1_3Dwaves, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE1",
					DIS_TYPE = "LINEAR 4 0 0.00000E+00 1 0.00000E+00 2 0.00000E+00 3 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_bc_bloc(m1_3Dwaves, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE2",
					DIS_TYPE = "LINEAR 4 4 0.00000E+00 5 0.00000E+00 6 0.00000E+00 7 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_bc_bloc(m1_3Dwaves, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE3",
					DIS_TYPE = "LINEAR 4 8 0.00000E+00 9 0.00000E+00 10 0.00000E+00 11 0.00000E+00",
					TIM_TYPE = "CURVE 1")

# ddc ----------------------------------------------------------------------
m1_3Dwaves <- input_add_blocs_from_file(m1_3Dwaves,
                                        sim_basename = "m1_3Dwaves",
                                        filename = "m1_3Dwaves.ddc",
                                        file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi")

# gli ----------------------------------------------------------------------
m1_3Dwaves <- input_add_blocs_from_file(m1_3Dwaves,
                    sim_basename = "m1_3Dwaves",
					filename = "m1_3Dwaves.gli",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi")

# mmp ----------------------------------------------------------------------
m1_3Dwaves <- input_add_mmp_bloc(m1_3Dwaves, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3")

# msh ----------------------------------------------------------------------
m1_3Dwaves <- input_add_blocs_from_file(m1_3Dwaves,
                    sim_basename = "m1_3Dwaves",
					filename = "m1_3Dwaves.msh",
					file_dir = "ogs5_benchmarks/Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi")

# msp ----------------------------------------------------------------------
m1_3Dwaves <- input_add_msp_bloc(m1_3Dwaves, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0",
					ELASTICITY = "POISSION 0.25 YOUNGS_MODULUS 1 2.5E+10")

# num ----------------------------------------------------------------------
m1_3Dwaves <- input_add_num_bloc(m1_3Dwaves, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 1 1.0e-10 10000 1.0 101 4")

# out ----------------------------------------------------------------------
m1_3Dwaves <- input_add_out_bloc(m1_3Dwaves, out_name = "OUTPUT1",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1")

# pcs ----------------------------------------------------------------------
m1_3Dwaves <- input_add_pcs_bloc(m1_3Dwaves, pcs_name = "PROCESS1",
					PCS_TYPE = "DEFORMATION",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
m1_3Dwaves <- input_add_rfd_bloc(m1_3Dwaves, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(-1,
										2),
								value = c(1,
										1)))

# st ----------------------------------------------------------------------
m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE4",
					DIS_TYPE = "LINEAR_NEUMANN 4 12 0.00000E+00 13 0.00000E+00 14 0.00000E+00 15 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE5",
					DIS_TYPE = "LINEAR_NEUMANN 4 16 0.00000E+00 17 0.00000E+00 18 0.00000E+00 19 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE6",
					DIS_TYPE = "LINEAR_NEUMANN 4 20 -1.00000E+10 21 1.00000E+10 22 1.00000E+10 23 -1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM4",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE7",
					DIS_TYPE = "LINEAR_NEUMANN 4 24 0.00000E+00 25 0.00000E+00 26 0.00000E+00 27 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM5",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE8",
					DIS_TYPE = "LINEAR_NEUMANN 4 28 0.00000E+00 29 0.00000E+00 30 0.00000E+00 31 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM6",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE9",
					DIS_TYPE = "LINEAR_NEUMANN 4 32 1.00000E+10 33 -1.00000E+10 34 -1.00000E+10 35 1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM7",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE10",
					DIS_TYPE = "LINEAR_NEUMANN 4 36 0.00000E+00 37 0.00000E+00 38 0.00000E+00 39 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM8",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE11",
					DIS_TYPE = "LINEAR_NEUMANN 4 40 0.00000E+00 41 0.00000E+00 42 0.00000E+00 43 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM9",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE12",
					DIS_TYPE = "LINEAR_NEUMANN 4 44 -1.00000E+10 45 1.00000E+10 46 1.00000E+10 47 -1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM10",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE13",
					DIS_TYPE = "LINEAR_NEUMANN 4 48 0.00000E+00 49 0.00000E+00 50 0.00000E+00 51 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM11",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE14",
					DIS_TYPE = "LINEAR_NEUMANN 4 52 0.00000E+00 53 0.00000E+00 54 0.00000E+00 55 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM12",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE15",
					DIS_TYPE = "LINEAR_NEUMANN 4 56 1.00000E+10 57 -1.00000E+10 58 -1.00000E+10 59 1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM13",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE16",
					DIS_TYPE = "LINEAR_NEUMANN 4 60 -1.00000E+10 61 -1.00000E+10 62 1.00000E+10 63 1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM14",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE17",
					DIS_TYPE = "LINEAR_NEUMANN 4 64 -1.00000E+10 65 1.00000E+10 66 1.00000E+10 67 -1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM15",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE18",
					DIS_TYPE = "LINEAR_NEUMANN 4 68 0.00000E+00 69 0.00000E+00 70 0.00000E+00 71 0.00000E+00",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM16",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURFACE19",
					DIS_TYPE = "LINEAR_NEUMANN 4 72 1.00000E+10 73 1.00000E+10 74 -1.00000E+10 75 -1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM17",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "SURFACE SURFACE20",
					DIS_TYPE = "LINEAR_NEUMANN 4 76 1.00000E+10 77 -1.00000E+10 78 -1.00000E+10 79 1.00000E+10",
					TIM_TYPE = "CURVE 1")

m1_3Dwaves <- input_add_st_bloc(m1_3Dwaves, st_name = "SOURCE_TERM18",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURFACE21",
					DIS_TYPE = "LINEAR_NEUMANN 4 80 0.00000E+00 81 0.00000E+00 82 0.00000E+00 83 0.00000E+00",
					TIM_TYPE = "CURVE 1")

# tim ----------------------------------------------------------------------
m1_3Dwaves <- input_add_tim_bloc(m1_3Dwaves, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION",
					TIME_UNIT = "SECOND",
					TIME_STEPS = "2 1.0",
					TIME_END = "1.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(m1_3Dwaves, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = m1_3Dwaves, ogs_exe = "ogs-mpi",
         use_mpi = TRUE,
         number_of_cores = 2,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi/log")
