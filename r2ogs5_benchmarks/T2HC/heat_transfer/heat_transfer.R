heat <- create_ogs5(sim_name = "heat",
					sim_path = "r2ogs5_benchmarks/tmp/T2HC/heat_transfer/",
					sim_id = 1L)

# gli ----------------------------------------------------------------------
heat <- input_add_blocs_from_file(heat,
                    sim_basename = "heat_transfer",
					filename = "heat_transfer.gli",
					file_dir = "ogs5_benchmarks/T2HC/heat_transfer/")

# ic ----------------------------------------------------------------------
heat <- input_add_ic_bloc(heat, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "TNEQ",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e+05"
)
heat <- input_add_ic_bloc(heat, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "TNEQ",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 773.15"
)
heat <- input_add_ic_bloc(heat, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "TNEQ",
					PRIMARY_VARIABLE = "TEMPERATURE2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 473.15"
)
heat <- input_add_ic_bloc(heat, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "TNEQ",
					PRIMARY_VARIABLE = "CONCENTRATION1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mcp ----------------------------------------------------------------------
heat <- input_add_mcp_bloc(heat, NAME = "N2",
					TRANSPORT_PHASE = "0",
					FLUID_PHASE = "0",
					MOBILE = "1",
					DIFFUSION = "1 9.6500e-05",
					MOL_MASS = "0.028"
)
heat <- input_add_mcp_bloc(heat, NAME = "H2O",
					TRANSPORT_PHASE = "0",
					FLUID_PHASE = "0",
					MOBILE = "1",
					DIFFUSION = "1 9.6500e-05",
					MOL_MASS = "0.018"
)
# mfp ----------------------------------------------------------------------
heat <- input_add_mfp_bloc(heat, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "26",
					VISCOSITY = "26",
					SPECIFIC_HEAT_CAPACITY = "1 1012",
					HEAT_CONDUCTIVITY = "11"
)
# mmp ----------------------------------------------------------------------
heat <- input_add_mmp_bloc(heat, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "0.0314",
					POROSITY = "1 0.5",
					TORTUOSITY = "1 1.000000e+000",
					MASS_DISPERSION = "1 0.1 0.01",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.94e-14",
					HEAT_TRANSFER = "1 1000",
					INTERPHASE_FRICTION = "FLUID"
)
# msh ----------------------------------------------------------------------
heat <- input_add_blocs_from_file(heat,
                    sim_basename = "heat_transfer",
					filename = "heat_transfer.msh",
					file_dir = "ogs5_benchmarks/T2HC/heat_transfer/")

# msp ----------------------------------------------------------------------
heat <- input_add_msp_bloc(heat, NAME = "SOLID_PROPERTIES1",
					DENSITY = c("1 1.0",
					            "CAPACITY:", "1 1.2e+003",
					            "CONDUCTIVITY:","1 0.4"),
					REACTIVE_SYSTEM = "INERT"
)
# num ----------------------------------------------------------------------
heat <- input_add_num_bloc(heat, num_name = "NUMERICS1",
					PCS_TYPE = "TNEQ",
					# REACTION_SCALING = ! is not a valid skey !"1",
					LOCAL_PICARD1 = "200 1.0e-10",
					LINEAR_SOLVER = "2 6 1.0e-015 1000 1.0 101 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-05 200 1.0",
					NON_LINEAR_UPDATE_VELOCITY = "1",
					OVERALL_COUPLING = "20 1.0e-03"
)
# out ----------------------------------------------------------------------
heat <- input_add_out_bloc(heat, out_name = "OUTPUT1",
					NOD_VALUES = c("PRESSURE1", "TEMPERATURE1", "TEMPERATURE2",
					                "CONCENTRATION1"),
					MFP_VALUES = "DENSITY",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "PVD",
					TIM_TYPE = "STEPS 50"
)
# pcs ----------------------------------------------------------------------
heat <- input_add_pcs_bloc(heat, pcs_name = "PROCESS1",
					PCS_TYPE = "TNEQ",
					# TEMPERATURE_UNIT = ! is not a valid skey !"KELVIN",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------
heat <- input_add_rfd_bloc(heat, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0,
										5.0,
										100000),
								value = c(0.0,
										1.0,
										1.0))
)
# tim ----------------------------------------------------------------------
heat <- input_add_tim_bloc(heat, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "TNEQ",
					TIME_STEPS = "5000 0.002",
					TIME_END = "1.0",
					TIME_START = "0.0"
)

ogs5_write_inputfiles(heat, "all")
ogs5_run(heat, "ogs_fem")
