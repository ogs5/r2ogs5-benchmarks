diff <- create_ogs5(sim_name = "diff",
					sim_path = "r2ogs5_benchmarks/tmp/T/TDiff/",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
diff <- input_add_bc_bloc(diff, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1"
)
# gli ----------------------------------------------------------------------
diff <- input_add_blocs_from_file(diff,
                    sim_basename = "TDiff",
					filename = "TDiff.gli",
					file_dir = "ogs5_benchmarks/T/TDiff/")

# ic ----------------------------------------------------------------------
diff <- input_add_ic_bloc(diff, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
diff <- input_add_ic_bloc(diff, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1"
)
# mfp ----------------------------------------------------------------------
diff <- input_add_mfp_bloc(diff, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 0.0",
					SPECIFIC_HEAT_CAPACITY = "1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0"
)
# mmp ----------------------------------------------------------------------
diff <- input_add_mmp_bloc(diff, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.10",
					STORAGE = "1 0.0",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0e-15",
					HEAT_DISPERSION = "1 0.000000e+000 0.000000e+000"
)
# msh ----------------------------------------------------------------------
diff <- input_add_blocs_from_file(diff,
                    sim_basename = "TDiff",
					filename = "TDiff.msh",
					file_dir = "ogs5_benchmarks/T/TDiff/")

# msp ----------------------------------------------------------------------
diff <- input_add_msp_bloc(diff, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2500",
					THERMAL = c("EXPANSION:", "1.0e-5", "CAPACITY:", "1 1000",
					            "CONDUCTIVITY:", "1 3.2")
)
# num ----------------------------------------------------------------------
diff <- input_add_num_bloc(diff, num_name = "NUMERICS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 0 1.e-012 1000 1.0 100 4",
					ELE_GAUSS_POINTS = "2",
					NON_LINEAR_SOLVER = "PICARD 1e-3 25 0.0"
)
# out ----------------------------------------------------------------------
diff <- input_add_out_bloc(diff, out_name = "OUTPUT1",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE ROCK",
					TIM_TYPE = "STEPS 1",
					DAT_TYPE = "TECPLOT"
)
diff <- input_add_out_bloc(diff, out_name = "OUTPUT2",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT2",
					TIM_TYPE = "STEPS 1",
					DAT_TYPE = "TECPLOT"
)
diff <- input_add_out_bloc(diff, out_name = "OUTPUT3",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT3",
					TIM_TYPE = "STEPS 1",
					DAT_TYPE = "TECPLOT"
)
diff <- input_add_out_bloc(diff, out_name = "OUTPUT4",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT4",
					TIM_TYPE = "STEPS 1",
					DAT_TYPE = "TECPLOT"
)
diff <- input_add_out_bloc(diff, out_name = "OUTPUT5",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT5",
					TIM_TYPE = "STEPS 1",
					DAT_TYPE = "TECPLOT"
)
# pcs ----------------------------------------------------------------------
diff <- input_add_pcs_bloc(diff, pcs_name = "PROCESS1",
					PCS_TYPE = "HEAT_TRANSPORT"
)
# tim ----------------------------------------------------------------------
diff <- input_add_tim_bloc(diff, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "1000 390625e+0",
					TIME_END = "1E99",
					TIME_START = "0.0"
)

ogs5_write_inputfiles(diff, "all")
ogs5_run(diff, "ogs_fem")
