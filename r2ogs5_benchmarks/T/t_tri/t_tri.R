tri <- create_ogs5(sim_name = "tri",
					sim_path = "r2ogs5_benchmarks/tmp/T/t_tri/",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
# gli ----------------------------------------------------------------------
tri <- input_add_blocs_from_file(tri,
                    sim_basename = "t_tri",
					filename = "t_tri.gli",
					file_dir = "ogs5_benchmarks/T/")

# ic ----------------------------------------------------------------------
tri <- input_add_ic_bloc(tri, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
tri <- input_add_mfp_bloc(tri, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 0.0",
					VISCOSITY = "1 0.0",
					SPECIFIC_HEAT_CAPACITY = "1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0"
)
# mmp ----------------------------------------------------------------------
tri <- input_add_mmp_bloc(tri, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000"
)
# msh ----------------------------------------------------------------------
tri <- input_add_blocs_from_file(tri,
                    sim_basename = "t_tri",
					filename = "t_tri.msh",
					file_dir = "ogs5_benchmarks/T/")

# msp ----------------------------------------------------------------------
tri <- input_add_msp_bloc(tri, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 1.0
  CAPACITY
    1 1.0
  CONDUCTIVITY
    1 1.0"
)
# num ----------------------------------------------------------------------
tri <- input_add_num_bloc(tri, num_name = "NUMERICS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 1 1.e-012 1000 0.5 100 4"
)
# out ----------------------------------------------------------------------
tri <- input_add_out_bloc(tri, out_name = "OUTPUT1",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT4",
					DAT_TYPE = "TECPLOT"
)
tri <- input_add_out_bloc(tri, out_name = "OUTPUT2",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE R",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0 0.5 1.0"
)
# pcs ----------------------------------------------------------------------
tri <- input_add_pcs_bloc(tri, pcs_name = "PROCESS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					# TEMPERATURE_UNIT = ! is not a valid skey !"KELVIN"
)
# st ----------------------------------------------------------------------
tri <- input_add_st_bloc(tri, st_name = "SOURCE_TERM1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE T",
					DIS_TYPE = "CONSTANT_NEUMANN 1."
)
# tim ----------------------------------------------------------------------
tri <- input_add_tim_bloc(tri, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "200 0.005",
					TIME_END = "1.0",
					TIME_START = "0.0",
					TIME_UNIT = "SECOND"
)

ogs5_write_inputfiles(tri, type = "all")

ogs5_run(tri, ogs_exe = "ogs_fem", log_output = TRUE)
