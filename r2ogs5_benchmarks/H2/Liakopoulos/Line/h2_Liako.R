# Benchmark: h2_Liako

# define ogs5 obj ---------------------------------------------------------
h2_Liako <- create_ogs5(sim_name = "h2_Liako",
					sim_path = "r2ogs5_benchmarks/tmp/H2/Liakopoulos/Line",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
h2_Liako <- input_add_bc_bloc(h2_Liako, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 101325")

h2_Liako <- input_add_bc_bloc(h2_Liako, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 101325")

h2_Liako <- input_add_bc_bloc(h2_Liako, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0")

# gli ----------------------------------------------------------------------
h2_Liako <- input_add_blocs_from_file(h2_Liako,
                    sim_basename = "h2_Liako",
					filename = "h2_Liako.gli",
					file_dir = "ogs5_benchmarks/H2/Liakopoulos/Line")

# ic ----------------------------------------------------------------------
h2_Liako <- input_add_ic_bloc(h2_Liako, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0")

h2_Liako <- input_add_ic_bloc(h2_Liako, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 101325")

# mfp ----------------------------------------------------------------------
h2_Liako <- input_add_mfp_bloc(h2_Liako, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

h2_Liako <- input_add_mfp_bloc(h2_Liako, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					DENSITY = "7",
					VISCOSITY = "1 1.8e-005")

# mmp ----------------------------------------------------------------------
h2_Liako <- input_add_mmp_bloc(h2_Liako, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 2.975000e-001",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.500000e-013",
					PERMEABILITY_SATURATION = "0 2 66 0.2 1.0 3.0 0.0001",
					CAPILLARY_PRESSURE = "0 1")

# msh ----------------------------------------------------------------------
h2_Liako <- input_add_blocs_from_file(h2_Liako,
					filename = "h2_Liako.msh",
					sim_basename = "h2_Liako",
					file_dir = "ogs5_benchmarks/H2/Liakopoulos/Line")

# num ----------------------------------------------------------------------
h2_Liako <- input_add_num_bloc(h2_Liako, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "2 6 1.e-12 2000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-5 50 1.0",
					ELE_GAUSS_POINTS = "2")

# out ----------------------------------------------------------------------
h2_Liako <- input_add_out_bloc(h2_Liako, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 PRESSURE2 PRESSURE_W SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_X2 VELOCITY_Y2",
					GEO_TYPE = "POLYLINE left",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.001 5 10 20 30 60 120")

h2_Liako <- input_add_out_bloc(h2_Liako, out_name = "OUTPUT2",
					NOD_VALUES = "PRESSURE1 PRESSURE2 PRESSURE_W SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_X2 VELOCITY_Y2",
					GEO_TYPE = "POINT POINT0",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1")

# pcs ----------------------------------------------------------------------
h2_Liako <- input_add_pcs_bloc(h2_Liako, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0")

# rfd ----------------------------------------------------------------------
h2_Liako <- input_add_blocs_from_file(h2_Liako,
                   filename = "h2_Liako.rfd",
                   sim_basename = "h2_Liako",
                   file_dir = "ogs5_benchmarks/H2/Liakopoulos/Line")


# tim ----------------------------------------------------------------------
h2_Liako <- input_add_tim_bloc(h2_Liako, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					TIME_STEPS = "10 0.001 9 0.01 9 0.1 120 1.0",
					TIME_END = "120.0",
					TIME_UNIT = "MINUTE",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(h2_Liako, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = h2_Liako, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H2/Liakopoulos/Line/log")
