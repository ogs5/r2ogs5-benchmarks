# Benchmark: trans_bd_homo

# define ogs5 obj ---------------------------------------------------------
trans_bd_homo <- create_ogs5(sim_name = "trans_bd_homo",
					sim_path = "r2ogs5_benchmarks/tmp/GROUNDWATER_FLOW/Transient_flow",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
trans_bd_homo <- input_add_bc_bloc(trans_bd_homo, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCLEFT",
					DIS_TYPE = "LINEAR 2 0 1.0 BC_left_low 3 1.0 BC_left_high")

trans_bd_homo <- input_add_bc_bloc(trans_bd_homo, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCRIGHT",
					DIS_TYPE = "LINEAR 2 1 1.0 BC_right_low 2 1.0 BC_right_high")

trans_bd_homo <- input_add_bc_bloc(trans_bd_homo, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCUNTEN",
					DIS_TYPE = "LINEAR 2 0 1.0 BC_left_low 1 1.0 BC_right_low")

trans_bd_homo <- input_add_bc_bloc(trans_bd_homo, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCOBEN",
					DIS_TYPE = "LINEAR 2 2 1.0 BC_right_high 3 1.0 BC_left_high")

# fct ----------------------------------------------------------------------
trans_bd_homo <- input_add_blocs_from_file(trans_bd_homo,
                   sim_basename = "trans_bd_homo",
                   filename = "trans_bd_homo.fct",
                   file_dir = "ogs5_benchmarks/GROUNDWATER_FLOW/Transient_flow")


# gli ----------------------------------------------------------------------
trans_bd_homo <- input_add_blocs_from_file(trans_bd_homo,
                    sim_basename = "trans_bd_homo",
					filename = "trans_bd_homo.gli",
					file_dir = "ogs5_benchmarks/GROUNDWATER_FLOW/Transient_flow")

# ic ----------------------------------------------------------------------
trans_bd_homo <- input_add_ic_bloc(trans_bd_homo, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 2.0")

# mfp ----------------------------------------------------------------------
trans_bd_homo <- input_add_mfp_bloc(trans_bd_homo, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 1e-3",
					# HEAT_CAPACITY = ! is not a valid skey !"1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0")

# mmp ----------------------------------------------------------------------
trans_bd_homo <- input_add_mmp_bloc(trans_bd_homo, NAME = "Layer1",
					GEO_TYPE = "LAYER 1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.25",
					VOL_BIO = "1 0.01",
					VOL_MAT = "1 0.74",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 0.00215",
					MASS_DISPERSION = "1 0.250 0.05",
					# DENSITY = ! is not a valid skey !"1 2000.0"
					)

# msh ----------------------------------------------------------------------
trans_bd_homo <- input_add_blocs_from_file(trans_bd_homo,
                    sim_basename = "trans_bd_homo",
					filename = "trans_bd_homo.msh",
					file_dir = "ogs5_benchmarks/GROUNDWATER_FLOW/Transient_flow")

# msp ----------------------------------------------------------------------
trans_bd_homo <- input_add_msp_bloc(trans_bd_homo, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003")

# num ----------------------------------------------------------------------
trans_bd_homo <- input_add_num_bloc(trans_bd_homo, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2",
					ELE_GAUSS_POINTS = "3")

# out ----------------------------------------------------------------------
trans_bd_homo <- input_add_out_bloc(trans_bd_homo, out_name = "OUTPUT1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "432000 864000 2160000 4320000")

trans_bd_homo <- input_add_out_bloc(trans_bd_homo, out_name = "OUTPUT2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "VTK",
					TIM_TYPE = "432000 864000 2160000 4320000")

# pcs ----------------------------------------------------------------------
trans_bd_homo <- input_add_pcs_bloc(trans_bd_homo, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW")

# st ----------------------------------------------------------------------
trans_bd_homo <- input_add_st_bloc(trans_bd_homo, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT -0.0e-05")

# tim ----------------------------------------------------------------------
trans_bd_homo <- input_add_tim_bloc(trans_bd_homo, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "10 432000.0",
					TIME_END = "144000.0e+60",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(trans_bd_homo, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = trans_bd_homo, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/GROUNDWATER_FLOW/Transient_flow/log")
