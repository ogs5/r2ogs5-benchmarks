# Benchmark: riv1_pris

# define ogs5 obj ---------------------------------------------------------
riv1_pris <- create_ogs5(sim_name = "riv1_pris",
					sim_path = "r2ogs5_benchmarks/tmp/GROUNDWATER_FLOW/riv1_pris",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
riv1_pris <- input_add_bc_bloc(riv1_pris, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCoben",
					DIS_TYPE = "CONSTANT 0")

riv1_pris <- input_add_bc_bloc(riv1_pris, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE BCunten",
					DIS_TYPE = "CONSTANT 0")

# gli ----------------------------------------------------------------------
riv1_pris <- input_add_blocs_from_file(riv1_pris,
					filename = "riv1_pris.gli",
					file_dir = "ogs5_benchmarks/GROUNDWATER_FLOW/riv1_pris")

# ic ----------------------------------------------------------------------
riv1_pris <- input_add_ic_bloc(riv1_pris, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0")

# mfp ----------------------------------------------------------------------
riv1_pris <- input_add_mfp_bloc(riv1_pris, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

# mmp ----------------------------------------------------------------------
riv1_pris <- input_add_mmp_bloc(riv1_pris, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000e+000",
					GEO_TYPE = "DOMAIN",
					POROSITY = "1 2.000000e-001",
					STORAGE = "1 2.000000e-001",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.50000e-002")

# msh ----------------------------------------------------------------------
riv1_pris <- input_add_blocs_from_file(riv1_pris,
					filename = "riv1_pris.msh",
					file_dir = "ogs5_benchmarks/GROUNDWATER_FLOW/riv1_pris")

# num ----------------------------------------------------------------------
riv1_pris <- input_add_num_bloc(riv1_pris, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					LINEAR_SOLVER = "2 1 1.e-10 1000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-10 100 0.0",
					ELE_GAUSS_POINTS = "2")

# out ----------------------------------------------------------------------
riv1_pris <- input_add_out_bloc(riv1_pris, out_name = "OUTPUT1",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS: 1")

riv1_pris <- input_add_out_bloc(riv1_pris, out_name = "OUTPUT2",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "POINT POINT4",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS: 1")

# pcs ----------------------------------------------------------------------
riv1_pris <- input_add_pcs_bloc(riv1_pris, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW")

# rfi ----------------------------------------------------------------------


# st ----------------------------------------------------------------------
riv1_pris <- input_add_st_bloc(riv1_pris, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE Channel",
					DIS_TYPE = "RIVER 2 0 4 1.00e-6 20.0 1.0 0.7 3 4 1.00e-6 20.0 1.0 0.7")

# tim ----------------------------------------------------------------------
riv1_pris <- input_add_tim_bloc(riv1_pris, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "30 60",
					TIME_END = "1800",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(riv1_pris, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = riv1_pris, ogs_exe = "inst/ogs/ogs_5.8",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/GROUNDWATER_FLOW/riv1_pris/log")
