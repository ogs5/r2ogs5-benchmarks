# Benchmark: advdiff

# define ogs5 obj ---------------------------------------------------------
advdiff <- create_ogs5(sim_name = "advdiff",
					sim_path = "r2ogs5_benchmarks/tmp/GAS_FLOW/Tracertest/AdvDiff",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
advdiff <- input_add_bc_bloc(advdiff, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT_NUEMANN 0.0")

advdiff <- input_add_bc_bloc(advdiff, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "WATER1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0",
					TIM_TYPE = "CURVE 1")

# gli ----------------------------------------------------------------------
advdiff <- input_add_blocs_from_file(advdiff,
                    sim_basename = "advdiff",
					filename = "advdiff.gli",
					file_dir = "ogs5_benchmarks/GAS_FLOW/Tracertest/AdvDiff")

# ic ----------------------------------------------------------------------
advdiff <- input_add_ic_bloc(advdiff, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 101325")

advdiff <- input_add_ic_bloc(advdiff, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 318.15")

advdiff <- input_add_ic_bloc(advdiff, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "WATER1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0")

# mfp ----------------------------------------------------------------------
advdiff <- input_add_mfp_bloc(advdiff, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					COMPONENTS = "1 WATER1",
					EOS_TYPE = "CONSTANT",
					COMPRESSIBILITY = "1 0 1 0 1 0",
					JTC = "OFF",
					DENSITY = "1 30",
					VISCOSITY = "1 0.00001",
					SPECIFIC_HEAT_CAPACITY = "1 1000",
					HEAT_CONDUCTIVITY = "1 0.6",
					ISOTHERM = "1 0",
					DECAY = "1 0.0",
					DIFFUSION = "1 1.0e-06")

# mmp ----------------------------------------------------------------------
advdiff <- input_add_mmp_bloc(advdiff, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.1",
					TORTUOSITY = "1 1.0e+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0e-014")

# msh ----------------------------------------------------------------------
advdiff <- input_add_blocs_from_file(advdiff,
                    sim_basename = "advdiff",
					filename = "advdiff.msh",
					file_dir = "ogs5_benchmarks/GAS_FLOW/Tracertest/AdvDiff")

# msp ----------------------------------------------------------------------
advdiff <- input_add_msp_bloc(advdiff, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2000",
					THERMAL = "EXPANSION\n 1.0e-05\n CAPACITY\n 1 960\n CONDUCTIVITY\n 1 3.0")

# num ----------------------------------------------------------------------
advdiff <- input_add_num_bloc(advdiff, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "2 0 1.0e-15 2000 1 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-05 25 1")

# out ----------------------------------------------------------------------
advdiff <- input_add_out_bloc(advdiff, out_name = "OUTPUT1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					NOD_VALUES = "WATER1 PRESSURE1 TEMPERATURE1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1",
					TIM_TYPE = "TIME",
					GEO_TYPE = "POINT POINT2",
					DAT_TYPE = "TECPLOT")

advdiff <- input_add_out_bloc(advdiff, out_name = "OUTPUT2",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					NOD_VALUES = "WATER1 PRESSURE1 TEMPERATURE1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1",
					TIM_TYPE = "TIME",
					GEO_TYPE = "POINT POINT3",
					DAT_TYPE = "TECPLOT")

# pcs ----------------------------------------------------------------------
advdiff <- input_add_pcs_bloc(advdiff, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					# TEMPERATURE_UNIT = ! is not a valid skey !"KELVIN"
					)


# rfd ---------------------------------------------------------------------
advdiff <- input_add_blocs_from_file(advdiff,
                    sim_basename = "advdiff",
                    filename = "advdiff.rfd",
                    file_dir = "ogs5_benchmarks/GAS_FLOW/Tracertest/AdvDiff"
                    )

# st ----------------------------------------------------------------------
advdiff <- input_add_st_bloc(advdiff, st_name = "SOURCE_TERM1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0",
					TIM_TYPE = "CURVE 2")

# tim ----------------------------------------------------------------------
advdiff <- input_add_tim_bloc(advdiff, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					TIME_STEPS = "360 86400",
					TIME_END = "248832000",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(advdiff, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = advdiff, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/GAS_FLOW/Tracertest/AdvDiff/log")
