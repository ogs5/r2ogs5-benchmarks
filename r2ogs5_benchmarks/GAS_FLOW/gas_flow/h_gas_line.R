# Benchmark: h_gas_line

# define ogs5 obj ---------------------------------------------------------
h_gas_line <- create_ogs5(sim_name = "h_gas_line",
					sim_path = "r2ogs5_benchmarks/tmp/GAS_FLOW/gas_flow",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
h_gas_line <- input_add_bc_bloc(h_gas_line, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 1.01325e+05")

h_gas_line <- input_add_bc_bloc(h_gas_line, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 3e+006")

# gli ----------------------------------------------------------------------
h_gas_line <- input_add_blocs_from_file(h_gas_line,
                    sim_basename = "h_gas_line",
					filename = "h_gas_line.gli",
					file_dir = "ogs5_benchmarks/GAS_FLOW/gas_flow")

# ic ----------------------------------------------------------------------
h_gas_line <- input_add_ic_bloc(h_gas_line, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.01325e+05")

h_gas_line <- input_add_ic_bloc(h_gas_line, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 288.15")

# mfp ----------------------------------------------------------------------
h_gas_line <- input_add_mfp_bloc(h_gas_line, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					COMPONENTS = "0",
					EOS_TYPE = "CONSTANT",
					COMPRESSIBILITY = "7 0 1 0 1 0",
					JTC = "OFF",
					DENSITY = "7",
					VISCOSITY = "1 1.7600e-05",
					SPECIFIC_HEAT_CAPACITY = "1 1000",
					HEAT_CONDUCTIVITY = "1 0.025")

# mmp ----------------------------------------------------------------------
h_gas_line <- input_add_mmp_bloc(h_gas_line, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.35",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.70000e-011")

# msh ----------------------------------------------------------------------
h_gas_line <- input_add_blocs_from_file(h_gas_line,
                    sim_basename = "h_gas_line",
					filename = "h_gas_line.msh",
					file_dir = "ogs5_benchmarks/GAS_FLOW/gas_flow")

# msp ----------------------------------------------------------------------
h_gas_line <- input_add_msp_bloc(h_gas_line, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.65e3",
					THERMAL = "EXPANSION\n 1e-5\n CAPACITY\n 1 1.2e+003\n CONDUCTIVITY\n 1 2.5")

# num ----------------------------------------------------------------------
h_gas_line <- input_add_num_bloc(h_gas_line, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "2 0 1.0e-15 2500 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-03 30 1.0")

# out ----------------------------------------------------------------------
h_gas_line <- input_add_out_bloc(h_gas_line, out_name = "OUTPUT1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					NOD_VALUES = "PRESSURE1 TEMPERATURE1",
					GEO_TYPE = "POLYLINE OUT",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "100000")

# pcs ----------------------------------------------------------------------
h_gas_line <- input_add_pcs_bloc(h_gas_line, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					#TEMPERATURE_UNIT = "KELVIN"
					)

# st ----------------------------------------------------------------------
h_gas_line <- input_add_st_bloc(h_gas_line, st_name = "SOURCE_TERM1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0.83447245336")

# tim ----------------------------------------------------------------------
h_gas_line <- input_add_tim_bloc(h_gas_line, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_COMPONENTIAL_FLOW",
					TIME_STEPS = "1 1.000000e+000 1 0.900000e+001 1 0.900000e+002 1 0.900000e+003 11 0.900000e+004",
					TIME_END = "1e+6",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(h_gas_line, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = h_gas_line, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/GAS_FLOW/gas_flow/log")
