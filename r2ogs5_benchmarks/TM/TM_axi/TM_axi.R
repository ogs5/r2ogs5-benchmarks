ogs5_obj <- create_ogs5(sim_name = "ogs5_obj",
					sim_path = "r2ogs5_benchmarks/THM/TM_axi/",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE PLY_B",
					DIS_TYPE = "CONSTANT 0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE PLY_T",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE PLY_O",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE PLY_O",
					DIS_TYPE = "CONSTANT 25.0"
)
# gli ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "TM_axi",
					filename = "TM_axi.gli",
					file_dir = "ogs5_benchmarks/THM/"
)
# ic ----------------------------------------------------------------------
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
ogs5_obj <- input_add_mfp_bloc(ogs5_obj, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 2200.0",
					VISCOSITY = "1 0.001",
					SPECIFIC_HEAT_CAPACITY = "1 0.0",
					HEAT_CONDUCTIVITY = "1 5.5"
)
# mmp ----------------------------------------------------------------------
ogs5_obj <- input_add_mmp_bloc(ogs5_obj, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1"
)
# msh ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "TM_axi",
					filename = "TM_axi.msh",
					file_dir = "ogs5_benchmarks/THM/"
)
# msp ----------------------------------------------------------------------
ogs5_obj <- input_add_msp_bloc(ogs5_obj, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 -2000.0",
					THERMAL = c("EXPANSION 4.2e-5",
					            "CAPACITY", "1 0.",
					            "CONDUCTIVITY", "1 5.5"),
					ELASTICITY = c("POISSION 0.25",
					               "YOUNGS_MODULUS", "1 2500e6")
)
# num ----------------------------------------------------------------------
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 2 1.e-12 2000 1.0 100 4",
					ELE_GAUSS_POINTS = "3"
)
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 2 1.e-13 2000 1.0 100 4"
)
# out ----------------------------------------------------------------------
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT1",
					NOD_VALUES = c("DISPLACEMENT_X1", "DISPLACEMENT_Y1",
					               "TEMPERATURE1", "STRESS_XX", "STRESS_XY",
					               "STRESS_YY", "STRESS_ZZ", "STRAIN_XX",
					               "STRAIN_XY", "STRAIN_YY", "STRAIN_ZZ"),
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1",
					AMPLIFIER = "10.0"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT2",
					NOD_VALUES = c("DISPLACEMENT_X1", "DISPLACEMENT_Y1",
					               "TEMPERATURE1", "STRESS_XX", "STRESS_XY",
					               "STRESS_YY", "STRESS_ZZ", "STRAIN_XX",
					               "STRAIN_XY", "STRAIN_YY", "STRAIN_ZZ"),
					GEO_TYPE = "POLYLINE PLY_B",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS1",
					PCS_TYPE = "HEAT_TRANSPORT",
					NUM_TYPE = "NEW"
)
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS2",
					PCS_TYPE = "DEFORMATION",
					NUM_TYPE = "NEW"
)
# st ----------------------------------------------------------------------
ogs5_obj <- input_add_st_bloc(ogs5_obj, st_name = "SOURCE_TERM1",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE PLY_I",
					DIS_TYPE = "CONSTANT_NEUMANN 30"
)
# tim ----------------------------------------------------------------------
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "1 1",
					TIME_END = "1",
					TIME_START = "0.0"
)
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "DEFORMATION",
					TIME_STEPS = "1 1",
					TIME_END = "1.0",
					TIME_START = "0.0"
)


ogs5_write_inputfiles(ogs5_obj, "all")
ogs5_run(ogs5_obj, "inst/ogs/ogs_5.8jb")
