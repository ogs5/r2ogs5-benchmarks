ogs5_obj <- create_ogs5(sim_name = "ogs5_obj",
					sim_path = "r2ogs5_benchmarks/TH2M/th2m_quad/",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 1.0",
					TIM_TYPE = "CURVE 1"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE TOP_C",
					DIS_TYPE = "CONSTANT 70"
)
# gli ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "th2m_quad",
					filename = "th2m_quad.gli",
					file_dir = "ogs5_benchmarks/TH2M/")

# ic ----------------------------------------------------------------------
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0E+005"
)
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0E+005"
)
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 30.0"
)
# mfp ----------------------------------------------------------------------
ogs5_obj <- input_add_mfp_bloc(ogs5_obj, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.0e-3",
					SPECIFIC_HEAT_CAPACITY = "1 4.280000e+003",
					HEAT_CONDUCTIVITY = "1 6.000000e-001"
)
ogs5_obj <- input_add_mfp_bloc(ogs5_obj, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE2",
					DENSITY = "7",
					VISCOSITY = "1 1.8e-5",
					SPECIFIC_HEAT_CAPACITY = "1 1.01e+3",
					HEAT_CONDUCTIVITY = "1 0.026",
					PHASE_DIFFUSION = "1 2.13e-6"
)
# mmp ----------------------------------------------------------------------
ogs5_obj <- input_add_mmp_bloc(ogs5_obj, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.2",
					TORTUOSITY = "1 0.8",
					DIFFUSION = "273",
					PERMEABILITY_TENSOR = "ISOTROPIC 5e-14",
					PERMEABILITY_SATURATION = "0 4 0 5",
					CAPILLARY_PRESSURE = "0 3"
)
# msh ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "th2m_quad",
					filename = "th2m_quad.msh",
					file_dir = "ogs5_benchmarks/TH2M/")

# msp ----------------------------------------------------------------------
ogs5_obj <- input_add_msp_bloc(ogs5_obj, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2000.0",
					THERMAL = c("EXPANSION: 1.0e-5",
					            "CAPACITY:", "1 1.091000e+003",
					            "CONDUCTIVITY:", "1 0.4200000e+000"),
					ELASTICITY = c("POISSION 0.4",
					               "YOUNGS_MODULUS:", "1 3.0e+7"),
					PLASTICITY = c("DRUCKER-PRAGER",
					               "5e5", "-1.0e5", "30", "20", "0.0")
)
# num ----------------------------------------------------------------------
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					LINEAR_SOLVER = "2 2 1.e-10 2000 1.0 100 4",
					ELE_MASS_LUMPING = "0",
					ELE_GAUSS_POINTS = "2",
					NON_LINEAR_SOLVER = "PICARD 1.0e-4 30 0.0",
					COUPLING_CONTROL = "LMAX 1.e-3 1.e-3",
					COUPLED_PROCESS = "HEAT_TRANSPORT 1 3",
					OVERALL_COUPLING = "1 1"
)
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 1 1.e-012 1000 0.5 100 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-6 100 0.0",
					COUPLING_CONTROL = "LMAX 1.e-6",
					OVERALL_COUPLING = "1 1"
)
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS3",
					PCS_TYPE = "DEFORMATION",
					NON_LINEAR_SOLVER = "NEWTON 1e-2 1e-10 100 0.0",
					LINEAR_SOLVER = "2 0 1.e-011 2000 1.0 100 4",
					COUPLING_CONTROL = "LMAX 1.e-3",
					OVERALL_COUPLING = "1 1"
)
# out ----------------------------------------------------------------------
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NOD_VALUES = c("PRESSURE1", "PRESSURE2", "PRESSURE_W",
					               "SATURATION1", "VELOCITY_X1", "VELOCITY_Y1",
					               "VELOCITY_X2", "VELOCITY_Y2"),
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT2",
					PCS_TYPE = "DEFORMATION",
					NOD_VALUES = c("DISPLACEMENT_X1", "DISPLACEMENT_Y1",
					               "STRESS_XX","STRESS_XY", "STRESS_YY",
					               "STRESS_ZZ", "STRAIN_XX", "STRAIN_XY",
					               "STRAIN_YY", "STRAIN_PLS"),
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT3",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT4",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NOD_VALUES = c("PRESSURE1", "PRESSURE2", "PRESSURE_W",
					               "SATURATION1", "VELOCITY_X1", "VELOCITY_Y1",
					               "VELOCITY_X2", "VELOCITY_Y2"),
					GEO_TYPE = "POINT POINT6",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT5",
					PCS_TYPE = "DEFORMATION",
					NOD_VALUES = c("DISPLACEMENT_X1", "DISPLACEMENT_Y1",
					               "STRESS_XX", "STRESS_XY", "STRESS_YY",
					               "STRESS_ZZ", "STRAIN_XX", "STRAIN_XY",
					               "STRAIN_YY", "STRAIN_PLS"),
					GEO_TYPE = "POINT POINT6",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT6",
					PCS_TYPE = "HEAT_TRANSPORT",
					NOD_VALUES = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT6",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					NUM_TYPE = "NEW"
)
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS3",
					PCS_TYPE = "DEFORMATION"
)
# rfd ----------------------------------------------------------------------
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES2",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0,
										1000.0),
								value = c(0.,
										-1.3))
)
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES3",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										1200.0,
										2400.0,
										4800.0,
										9600.0),
								value = c(1.e5,
										1.e5,
										1.e5,
										1.e5,
										1.e5))
)
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES4",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(2.000000030E-001,
										2.018283159E-001,
										2.399999946E-001,
										3.065373600E-001,
										4.000000060E-001,
										4.967313409E-001,
										6.000000238E-001,
										6.934626698E-001,
										8.049030304E-001,
										8.640000224E-001,
										9.169030190E-001,
										9.599030018E-001,
										9.759999514E-001,
										9.948059916E-001,
										9.979029894E-001,
										1.000000000E+000),
								value = c(6.400000000E+005,
										5.987272500E+005,
										4.000000000E+005,
										3.250909375E+005,
										2.700000000E+005,
										2.349090781E+005,
										2.000000000E+005,
										1.756363438E+005,
										1.414545938E+005,
										1.225454688E+005,
										1.038181953E+005,
										8.381819531E+004,
										6.872726563E+004,
										4.618180469E+004,
										2.409098633E+004,
										0.000000000E+000))
)
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES5",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(2.000000030E-001,
										2.399999946E-001,
										2.647727430E-001,
										3.000000119E-001,
										4.000000060E-001,
										4.499999881E-001,
										5.000000000E-001,
										6.000000238E-001,
										6.999999881E-001,
										8.000000119E-001,
										8.500000238E-001,
										8.999999762E-001,
										9.100000262E-001,
										9.200000167E-001,
										9.399999976E-001,
										9.499999881E-001,
										9.637847543E-001,
										9.700000286E-001,
										9.875695109E-001,
										9.937847853E-001,
										1.000000000E+000),
								value = c(1.000000000E+000,
										8.899999857E-001,
										8.168712258E-001,
										7.200000286E-001,
										5.000000000E-001,
										4.000000060E-001,
										3.100000024E-001,
										1.700000018E-001,
										7.000000030E-002,
										1.700000092E-002,
										1.499999966E-002,
										1.700000092E-002,
										1.882802136E-002,
										2.500000037E-002,
										5.000000075E-002,
										1.000000015E-001,
										1.948159337E-001,
										3.000000119E-001,
										5.982720256E-001,
										8.000000119E-001,
										1.000000000E+000))
)
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES6",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(2.000000030E-001,
										2.399999946E-001,
										2.647727430E-001,
										3.000000119E-001,
										4.000000060E-001,
										4.499999881E-001,
										5.000000000E-001,
										6.000000238E-001,
										7.111498713E-001,
										8.015800714E-001,
										8.563649654E-001,
										9.063808322E-001,
										9.495475292E-001,
										1.001592875E+000),
								value = c(0.99,
										8.899999857E-001,
										8.168712258E-001,
										7.200000286E-001,
										5.000000000E-001,
										4.000000060E-001,
										3.100000024E-001,
										1.700000018E-001,
										8.593542129E-002,
										4.099307209E-002,
										2.260014415E-002,
										1.251126267E-002,
										7.170357276E-003,
										0.000000000E+000))
)
# tim ----------------------------------------------------------------------
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					TIME_END = "12.0",
					TIME_START = "0.0",
					TIME_UNIT = "MINUTE",
					TIME_CONTROL = c("PI_AUTO_STEP_SIZE",
					                 "1 1.0e-3 1.0e-9 0.01")
)
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "12 1.",
					TIME_END = "12.0",
					TIME_UNIT = "MINUTE",
					TIME_START = "0.0",
					TIME_CONTROL = ""
)
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING3",
					PCS_TYPE = "DEFORMATION",
					TIME_STEPS = "12 1.0",
					TIME_END = "12.0",
					TIME_START = "0.0",
					TIME_CONTROL = ""
)

ogs5_write_inputfiles(ogs5_obj, "all")
ogs5_run(ogs5_obj, "inst/ogs/ogs_5.8jb")
