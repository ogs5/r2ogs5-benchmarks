growth <- create_ogs5(sim_name = "growth",
					sim_path = "r2ogs5_benchmarks/tmp/TCR/Temperature_BacGrowth/",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.005"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 1.00"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_A",
					GEO_TYPE = "POINT POINT0",
					EPSILON = "0.00001",
					DIS_TYPE = "CONSTANT 0.2"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_B",
					GEO_TYPE = "POINT POINT0",
					EPSILON = "0.00001",
					DIS_TYPE = "CONSTANT 1.35"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "POINT POINT0",
					EPSILON = "0.00001",
					DIS_TYPE = "CONSTANT 0.2"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 305.15"
)
growth <- input_add_bc_bloc(growth, bc_name = "BOUNDARY_CONDITION7",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 305.15"
)
# gli ----------------------------------------------------------------------
growth <- input_add_blocs_from_file(growth,
                    sim_basename = "bact_growth_new",
					filename = "bact_growth_new.gli",
					file_dir = "ogs5_benchmarks/TCR/Temperature_BacGrowth/")

# ic ----------------------------------------------------------------------
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_A",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_B",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.00"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_C",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_D",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION6",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Comp_E",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION7",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION8",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Biomass",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1e-9"
)
growth <- input_add_ic_bloc(growth, ic_name = "INITIAL_CONDITION9",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 305.15"
)
# krc ----------------------------------------------------------------------
growth <- input_add_krc_bloc(growth, krc_name = "KINREACTIONDATA1",
                    SOLVER_TYPE = "1",
                    RELATIVE_ERROR = "1.e-6",
                    MIN_TIMESTEP = "1.e-6",
                    INITIAL_TIMESTEP = "1.0",
                    BACTERIACAPACITY = "1",
                    NO_REACTIONS = "POINT POINT0",
                    DEBUG_OUTPUT = "")

growth <- input_add_krc_bloc(growth, krc_name = "REACTION1",
                    NAME = "DoubleMonod",
                    TYPE = "monod",
                    BACTERIANAME = "Biomass",
                    EQUATION =
                     "1 Comp_A + 21 Comp_B = 8 Comp_C + 21 Comp_D + 8 Comp_E",
                    RATECONSTANT = "1.0 1.0",
                    TEMPERATURE_DEPENDENCE =
                        "1 290.15 313.15 0.2030012 0.0012304",
                    GROWTH = "1",
                    MONODTERMS = c("Comp_A 1.14e-02 1.0", "Comp_B 7.0e-02 1.0"),
                    INHIBITIONTERMS = "",
                    PRODUCTIONSTOCH = c("Comp_A -1.0", "Comp_B -21.0",
                                    "Comp_C +8.0", "Comp_D +21", "Comp_E +8")
)

growth <- input_add_krc_bloc(growth, krc_name = "REACTION2",
                               NAME = "DoubleMonodDecay",
                               TYPE = "monod",
                               BACTERIANAME = "Biomass",
                               EQUATION = "Biomass = Biomass",
                               RATECONSTANT = "-1.16e-6 1.0",
                               GROWTH = "1",
                               MONODTERMS = "",
                               INHIBITIONTERMS = "",
                               PRODUCTIONSTOCH = ""
)


# mcp ----------------------------------------------------------------------
growth <- input_add_mcp_bloc(growth, NAME = "ConsTracer",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Comp_A",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Comp_B",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Comp_C",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Comp_D",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Comp_E",
					MOBILE = "1",
					DIFFUSION = "1 0.0"
)
growth <- input_add_mcp_bloc(growth, NAME = "Biomass",
					MOBILE = "0",
					DIFFUSION = "1 0.0",
					TRANSPORT_PHASE = "2"
)
# mfp ----------------------------------------------------------------------
growth <- input_add_mfp_bloc(growth, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 995.0292",
					VISCOSITY = "1 1.3e-3"
)
# mmp ----------------------------------------------------------------------
growth <- input_add_mmp_bloc(growth, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.25",
					VOL_BIO = "1 0.01",
					VOL_MAT = "1 0.74",
					TORTUOSITY = "1 1.0",
					STORAGE = "1 0.000",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.8935e-3",
					MASS_DISPERSION = "1 0.5 2.5e-4",
					# DENSITY = ! is not a valid skey !"1 2650.0",
					HEAT_DISPERSION = "1 0.15 0.15"
)
# msh ----------------------------------------------------------------------
growth <- input_add_blocs_from_file(growth,
                    sim_basename = "bact_growth_new",
					filename = "bact_growth_new.msh",
					file_dir = "ogs5_benchmarks/TCR/Temperature_BacGrowth/")

# msp ----------------------------------------------------------------------
growth <- input_add_msp_bloc(growth, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.65000e+003",
					THERMAL = c("EXPANSION", "1 0.0",
					            "CAPACITY", "1 1640",
					            "CONDUCTIVITY", "1 2.47")
)
# num ----------------------------------------------------------------------
growth <- input_add_num_bloc(growth, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-010 1000 1.0 1 2"
)
growth <- input_add_num_bloc(growth, num_name = "NUMERICS2",
					PCS_TYPE = "MASS_TRANSPORT",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-010 1000 1.0 1 2"
)
growth <- input_add_num_bloc(growth, num_name = "NUMERICS3",
					PCS_TYPE = "HEAT_TRANSPORT",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-010 1000 1.0 1 2"
)
# out ----------------------------------------------------------------------
growth <- input_add_out_bloc(growth, out_name = "OUTPUT1",
					NOD_VALUES = c("Comp_A", "Comp_B", "Comp_C", "Comp_D",
					               "Comp_E", "Biomass", "ConsTracer",
					               "TEMPERATURE1"),
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 480"
)
growth <- input_add_out_bloc(growth, out_name = "OUTPUT2",
					NOD_VALUES = c("Comp_A", "Comp_B", "Comp_C", "Comp_D",
					               "Comp_E", "Biomass", "ConsTracer",
					               "TEMPERATURE1"),
					GEO_TYPE = "POINT POINT2",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 120"
)
growth <- input_add_out_bloc(growth, out_name = "OUTPUT3",
					NOD_VALUES = c("Comp_A", "Comp_B", "Comp_C", "Comp_D",
					               "Comp_E", "Biomass", "ConsTracer",
					               "TEMPERATURE1"),
					GEO_TYPE = "POINT POINT3",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 120"
)
growth <- input_add_out_bloc(growth, out_name = "OUTPUT4",
					NOD_VALUES = c("Comp_A", "Comp_B", "Comp_C", "Comp_D",
					               "Comp_E", "Biomass", "ConsTracer",
					               "TEMPERATURE1"),
					GEO_TYPE = "POINT POINT4",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 120"
)
growth <- input_add_out_bloc(growth, out_name = "OUTPUT5",
					NOD_VALUES = c("Comp_A", "Comp_B", "Comp_C", "Comp_D",
					               "Comp_E", "Biomass", "ConsTracer",
					               "TEMPERATURE1"),
					GEO_TYPE = "POINT POINT5",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 120"
)
# pcs ----------------------------------------------------------------------
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS3",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS4",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS5",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS6",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS7",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS8",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
growth <- input_add_pcs_bloc(growth, pcs_name = "PROCESS9",
					PCS_TYPE = "HEAT_TRANSPORT",
					# TEMPERATURE_UNIT = ! is not a valid skey !"KELVIN"
)
# rei ----------------------------------------------------------------------
growth <- input_add_rei_bloc(growth, rei_name = "REACTION_INTERFACE1",
					TEMPERATURE = "VARIABLE"
)
# tim ----------------------------------------------------------------------
growth <- input_add_tim_bloc(growth, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "2400 720",
					TIME_END = "17280000",
					TIME_START = "0.0"
)
growth <- input_add_tim_bloc(growth, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "2400 720",
					TIME_END = "17280000",
					TIME_START = "0.0"
)
growth <- input_add_tim_bloc(growth, tim_name = "TIME_STEPPING3",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "2400 720",
					TIME_END = "17280000",
					TIME_START = "0.0"
)

ogs5_write_inputfiles(growth, "all")
ogs5_run(growth, "ogs_fem")
