d_1 <- create_ogs5(sim_name = "d_1",
					sim_path = "r2ogs5_benchmarks/tmp/C/1d_analyt",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 2.000"
)
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "SURFACE: RIGHTSURF",
					DIS_TYPE = "CONSTANT: 1.000"
)
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Decay",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1.00"
)
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "SorbLin",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1.00"
)
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "DecaySorbnonlin",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1.0"
)
d_1 <- input_add_bc_bloc(d_1, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1.0"
)
# gli ----------------------------------------------------------------------
d_1 <- input_add_blocs_from_file(d_1,
					sim_basename = "1d_1",
					filename = "1d_1.gli",
					file_dir = "ogs5_benchmarks/C/1d_analyt")

# ic ----------------------------------------------------------------------
d_1 <- input_add_ic_bloc(d_1, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.00"
)
d_1 <- input_add_ic_bloc(d_1, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Decay",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
d_1 <- input_add_ic_bloc(d_1, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "SorbLin",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
d_1 <- input_add_ic_bloc(d_1, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "DecaySorbnonlin",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
d_1 <- input_add_ic_bloc(d_1, ic_name = "INITIAL_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mcp ----------------------------------------------------------------------
d_1 <- input_add_mcp_bloc(d_1, NAME = "Decay",
					MOBILE = "1",
					DIFFUSION = "1 1.0e-9",
					DECAY = "1 1.0e-7 1.0"
)
d_1 <- input_add_mcp_bloc(d_1, NAME = "SorbLin",
					MOBILE = "1",
					DIFFUSION = "1 1.0e-09",
					ISOTHERM = "2 1e-3 1.0"
)
d_1 <- input_add_mcp_bloc(d_1, NAME = "DecaySorbnonlin",
					MOBILE = "1",
					DIFFUSION = "1 3.0e-9",
					DECAY = "2 1.0e-7 1.0e-1",
					ISOTHERM = "3 1e-3 0.5"
)
d_1 <- input_add_mcp_bloc(d_1, NAME = "ConsTracer",
					MOBILE = "1",
					DIFFUSION = "1 1.0e-9"
)
# mfp ----------------------------------------------------------------------
d_1 <- input_add_mfp_bloc(d_1, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 1e-3",
					# HEAT_CAPACITY = ! is not a valid skey !"1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0"
)
# mmp ----------------------------------------------------------------------
d_1 <- input_add_mmp_bloc(d_1, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.500",
					TORTUOSITY = "1 1.000000e+000",
					STORAGE = "1 0.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 5.787037e-04",
					MASS_DISPERSION = "1 0.25 0.25",
					# DENSITY = ! is not a valid skey !"1 2000.0"
)
# msh ----------------------------------------------------------------------
d_1 <- input_add_blocs_from_file(d_1,
					sim_basename = "1d_1",
					filename = "1d_1.msh",
					file_dir = "ogs5_benchmarks/C/1d_analyt")

# msp ----------------------------------------------------------------------
d_1 <- input_add_msp_bloc(d_1, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003"
)
# num ----------------------------------------------------------------------
d_1 <- input_add_num_bloc(d_1, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
d_1 <- input_add_num_bloc(d_1, num_name = "NUMERICS2",
					PCS_TYPE = "FLUID_MOMENTUM",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
d_1 <- input_add_num_bloc(d_1, num_name = "NUMERICS3",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 6 1.e-014 1000 0.5 1 2",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
d_1 <- input_add_out_bloc(d_1, out_name = "OUTPUT1",
					NOD_VALUES = "HEAD Decay SorbLin DecaySorbnonlin ConsTracer",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0 86400 2160000 4320000 6480000 8640000"
)
d_1 <- input_add_out_bloc(d_1, out_name = "OUTPUT2",
					NOD_VALUES = "HEAD Decay SorbLin DecaySorbnonlin ConsTracer",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0 86400 2160000 4320000 6480000 8640000"
)
d_1 <- input_add_out_bloc(d_1, out_name = "OUTPUT3",
					NOD_VALUES = "HEAD Decay SorbLin DecaySorbnonlin ConsTracer",
					GEO_TYPE = "POINT POINT8",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
d_1 <- input_add_pcs_bloc(d_1, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW"
)
d_1 <- input_add_pcs_bloc(d_1, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "1"
)
d_1 <- input_add_pcs_bloc(d_1, pcs_name = "PROCESS3",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "1"
)
d_1 <- input_add_pcs_bloc(d_1, pcs_name = "PROCESS4",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "1"
)
d_1 <- input_add_pcs_bloc(d_1, pcs_name = "PROCESS5",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "1"
)
# st ----------------------------------------------------------------------
d_1 <- input_add_st_bloc(d_1, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "SURFACE RIGHTSURF",
					DIS_TYPE = "CONSTANT_NEUMANN 0.0"
)
# tim ----------------------------------------------------------------------
d_1 <- input_add_tim_bloc(d_1, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "100 86400.0",
					TIME_END = "8640000.0",
					TIME_START = "0.0"
)
d_1 <- input_add_tim_bloc(d_1, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "100 86400.0",
					TIME_END = "8640000.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(d_1, "all")
# run ogs5 simulation----------------------------------------------------------------------
ogs5_run(d_1,
         run_path = NULL,
		 log_output = TRUE,
		 log_path = "r2ogs5_benchmarks/tmp/C/1d_analyt/log")
