enges1 <- create_ogs5(sim_name = "enges1",
					sim_path = "r2ogs5_benchmarks/tmp/C/Engesgaard/2Kin/slow_kin_pqc/tmp",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0e5"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "C(4)",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0e-7"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Ca",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0e-7"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Mg",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.00"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Cl",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 2.0"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "pH",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 7.0"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION7",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "pe",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 4.0"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION8",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Calcite",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 4.70588E-10"
)
enges1 <- input_add_bc_bloc(enges1, bc_name = "BOUNDARY_CONDITION9",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Dolomite(dis)",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 4.70588E-10"
)
# gli ----------------------------------------------------------------------
enges1 <- input_add_blocs_from_file(enges1,
					sim_basename = "pds",
					filename = "pds.gli",
					file_dir = "ogs5_benchmarks/C/Engesgaard/2Kin/slow_kin_pqc")

# ic ----------------------------------------------------------------------
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "C(4)",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.23e-1"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Ca",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.23e-1"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Mg",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e-9"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Cl",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e-9"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION6",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "pH",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 9.91"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION7",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "pe",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 4.0"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION8",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Calcite",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 5.7412E-02"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION9",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Dolomite(dis)",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
enges1 <- input_add_ic_bloc(enges1, ic_name = "INITIAL_CONDITION10",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "CO3-2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e-4"
)
# mcp ----------------------------------------------------------------------
enges1 <- input_add_mcp_bloc(enges1, NAME = "C(4)",
					MOBILE = "1",
					DIFFUSION = "1 0.0e-9",
					VALENCE = "-2"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "Ca",
					MOBILE = "1",
					DIFFUSION = "1 0.0e-9",
					VALENCE = "+2"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "Mg",
					MOBILE = "1",
					DIFFUSION = "1 0.0e-9",
					VALENCE = "+2"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "Cl",
					MOBILE = "1",
					DIFFUSION = "1 0.0e-9",
					VALENCE = "-1"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "pH",
					MOBILE = "0",
					DIFFUSION = "0"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "pe",
					MOBILE = "0",
					DIFFUSION = "0"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "CO3-2",
					MOBILE = "1",
					DIFFUSION = "1 0.0e-9",
					VALENCE = "-2"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "Calcite",
					MOBILE = "0",
					DIFFUSION = "0",
					TRANSPORT_PHASE = "1"
)
enges1 <- input_add_mcp_bloc(enges1, NAME = "Dolomite(dis)",
					MOBILE = "0",
					DIFFUSION = "0",
					TRANSPORT_PHASE = "1"
)
# mfp ----------------------------------------------------------------------
enges1 <- input_add_mfp_bloc(enges1, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 1.0e-3",
					# HEAT_CAPACITY = ! is not a valid skey !"1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0"
)
# mmp ----------------------------------------------------------------------
enges1 <- input_add_mmp_bloc(enges1, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.32",
					VOL_MAT = "1 0.68",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.15700e-12",
					MASS_DISPERSION = "1 0.0067 0.1000",
					# DENSITY = ! is not a valid skey !"1 1800.0"
)
# msh ----------------------------------------------------------------------
enges1 <- input_add_blocs_from_file(enges1,
					sim_basename = "pds",
					filename = "pds.msh",
					file_dir = "ogs5_benchmarks/C/Engesgaard/2Kin/slow_kin_pqc")

# msp ----------------------------------------------------------------------
enges1 <- input_add_msp_bloc(enges1, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 1.80000e+003"
)
# num ----------------------------------------------------------------------
enges1 <- input_add_num_bloc(enges1, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
enges1 <- input_add_num_bloc(enges1, num_name = "NUMERICS2",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 6 1.e-014 1000 0.5 1 2",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
enges1 <- input_add_out_bloc(enges1, out_name = "OUTPUT1",
					NOD_VALUES = "C(4) Ca Mg Cl pH pe Calcite Dolomite(dis)",
					GEO_TYPE = "POINT POINT2",
					DAT_TYPE = "TECPLOT"
)
enges1 <- input_add_out_bloc(enges1, out_name = "OUTPUT2",
					NOD_VALUES = "HEAD C(4) Ca Mg Cl pH pe Calcite Dolomite(dis)",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.0 100.0 1000.0 10000.0 21000.0"
)
enges1 <- input_add_out_bloc(enges1, out_name = "OUTPUT3",
					NOD_VALUES = "HEAD C(4) Ca Mg Cl pH pe Calcite Dolomite(dis)",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.0 100.0 1000.0 10000.0 21000.0"
)
enges1 <- input_add_out_bloc(enges1, out_name = "OUTPUT4",
					NOD_VALUES = "HEAD CO3-2 Ca Mg Cl pH pe Calcite Dolomite(dis)",
					GEO_TYPE = "POLYLINE OUT_LINE2",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.0 100.0 1000.0 10000.0 21000.0"
)
# pcs ----------------------------------------------------------------------
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS3",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS4",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS5",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS6",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS7",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS8",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS9",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
enges1 <- input_add_pcs_bloc(enges1, pcs_name = "PROCESS10",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
# pqc ----------------------------------------------------------------------
enges1 <- input_add_blocs_from_file(enges1,
                          sim_basename = "pds",
                          filename = "pds.pqc",
                          file_dir = "ogs5_benchmarks/C/Engesgaard/2Kin/slow_kin_pqc")
# rei ----------------------------------------------------------------------
enges1 <- input_add_rei_bloc(enges1, rei_name = "REACTION_INTERFACE1",
					MOL_PER = "VOLUME",
					WATER_CONCENTRATION = "CONSTANT",
					PRESSURE = "CONSTANT 1.0",
					TEMPERATURE = "CONSTANT 298.15"
)
# st ----------------------------------------------------------------------
enges1 <- input_add_st_bloc(enges1, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT -2.9976852e-006"
)
# tim ----------------------------------------------------------------------
enges1 <- input_add_tim_bloc(enges1, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "210 100.0",
					TIME_END = "21000.0",
					TIME_START = "0.0"
)
enges1 <- input_add_tim_bloc(enges1, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "210 100.0",
					TIME_END = "21000.0",
					TIME_START = "0.0"
)
# dat ----------------------------------------------------------------------
enges1 <- input_add_blocs_from_file(enges1,
                          sim_basename = "pds",
                          filename = "phreeqc.dat",
                          file_dir = "ogs5_benchmarks/C/Engesgaard/2Kin/slow_kin_pqc")


# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(enges1,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(enges1, ogs_exe = "ogs-ipqc",
			run_path = NULL,
			log_output = TRUE,
			log_path = "r2ogs5_benchmarks/tmp/C/Engesgaard/2Kin/slow_kin_pqc/log")
