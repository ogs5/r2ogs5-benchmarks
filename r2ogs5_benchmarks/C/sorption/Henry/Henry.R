henry <- create_ogs5(sim_name = "henry",
					sim_path = "r2ogs5_benchmarks/tmp/C/sorption/Henry",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
henry <- input_add_bc_bloc(henry, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT: 140000"
)
henry <- input_add_bc_bloc(henry, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT: 20000"
)
henry <- input_add_bc_bloc(henry, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "SorbFr",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1"
)
henry <- input_add_bc_bloc(henry, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "SURFACE: LEFTSURF",
					DIS_TYPE = "CONSTANT: 1.0"
)
# gli ----------------------------------------------------------------------
henry <- input_add_blocs_from_file(henry,
					sim_basename = "HC_sorp_henry_1D",
					filename = "HC_sorp_henry_1D.gli",
					file_dir = "ogs5_benchmarks/C/sorption/Henry")

# ic ----------------------------------------------------------------------
henry <- input_add_ic_bloc(henry, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
henry <- input_add_ic_bloc(henry, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "SorbFr",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
henry <- input_add_ic_bloc(henry, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "ConsTracer",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0"
)
# mcp ----------------------------------------------------------------------
henry <- input_add_mcp_bloc(henry, NAME = "SorbFr",
					MOBILE = "1",
					DIFFUSION = "1 0",
					ISOTHERM = "1 6.8e-4"
)
henry <- input_add_mcp_bloc(henry, NAME = "ConsTracer",
					MOBILE = "1",
					DIFFUSION = "1 0"
)
# mfp ----------------------------------------------------------------------
henry <- input_add_mfp_bloc(henry, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 1e-3",
					HEAT_CONDUCTIVITY = "1 0.0"
)
# mmp ----------------------------------------------------------------------
henry <- input_add_mmp_bloc(henry, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.200",
					TORTUOSITY = "1 1.000000e+000",
					STORAGE = "1 0.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 1e-12",
					MASS_DISPERSION = "1 5 0",
)
# msh ----------------------------------------------------------------------
henry <- input_add_blocs_from_file(henry,
					sim_basename = "HC_sorp_henry_1D",
					filename = "HC_sorp_henry_1D.msh",
					file_dir = "ogs5_benchmarks/C/sorption/Henry")

# msp ----------------------------------------------------------------------
henry <- input_add_msp_bloc(henry, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003"
)
# num ----------------------------------------------------------------------
henry <- input_add_num_bloc(henry, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
henry <- input_add_num_bloc(henry, num_name = "NUMERICS2",
					PCS_TYPE = "FLUID_MOMENTUM",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
henry <- input_add_num_bloc(henry, num_name = "NUMERICS3",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 6 1.e-014 1000 0.5 1 2",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
henry <- input_add_out_bloc(henry, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 SorbFr ConsTracer",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
henry <- input_add_out_bloc(henry, out_name = "OUTPUT2",
					NOD_VALUES = "PRESSURE1 SorbFr ConsTracer",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
henry <- input_add_out_bloc(henry, out_name = "OUTPUT3",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
henry <- input_add_pcs_bloc(henry, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					NUM_TYPE = "NEW"
)
henry <- input_add_pcs_bloc(henry, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
henry <- input_add_pcs_bloc(henry, pcs_name = "PROCESS3",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)

# tim ----------------------------------------------------------------------
henry <- input_add_tim_bloc(henry, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "100 86400.0",
					TIME_END = "8640000.0",
					TIME_START = "0.0"
)
henry <- input_add_tim_bloc(henry, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "100 86400.0",
					TIME_END = "8640000.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(henry, "all")
# run ogs5 simulation----------------------------------------------------------------------
ogs5_run(henry, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/C/sorption/Henry/log")
