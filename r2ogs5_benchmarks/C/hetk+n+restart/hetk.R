ogs5_obj <- create_ogs5(sim_name = "ogs5_obj",
					sim_path = "r2ogs5_benchmarks/tmp/C/hetk+n+restart",
					sim_id = 1L)

# dat ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
					sim_basename = "_het_k",
					filename = "_het_k.dat",
					file_dir = "ogs5_benchmarks/C/hetk+n+restart")

# dat ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
					sim_basename = "_het_k",
					filename = "_het_n.dat",
					file_dir = "ogs5_benchmarks/C/hetk+n+restart")

# dat ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
					sim_basename = "_het_k",
					filename = "_ic_heads.dat",
					file_dir = "ogs5_benchmarks/C/hetk+n+restart")

# bc ----------------------------------------------------------------------
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE L_BC",
					DIS_TYPE = "CONSTANT 10"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "POLYLINE BCTracer1",
					DIS_TYPE = "CONSTANT 1"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "POLYLINE BCTracer2",
					DIS_TYPE = "CONSTANT 1"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "POLYLINE BCWater3",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "POLYLINE BCWater2",
					DIS_TYPE = "CONSTANT 0.0"
)
ogs5_obj <- input_add_bc_bloc(ogs5_obj, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "POLYLINE BCWater1",
					DIS_TYPE = "CONSTANT 0.0"
)
# gli ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
					sim_basename = "2D1P_transport",
					filename = "2D1P_transport.gli",
					file_dir = "ogs5_benchmarks/C/hetk+n+restart")

# ic ----------------------------------------------------------------------
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "RESTART _ic_heads.dat"
)
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1e-10"
)
# mcp ----------------------------------------------------------------------
ogs5_obj <- input_add_mcp_bloc(ogs5_obj, NAME = "Tracer1",
					MOBILE = "1",
					DIFFUSION = "1 1.0e-9"
)
# mfp ----------------------------------------------------------------------
ogs5_obj <- input_add_mfp_bloc(ogs5_obj, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
ogs5_obj <- input_add_mmp_bloc(ogs5_obj, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.1",
					POROSITY_DISTRIBUTION = "_het_n.dat",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0",
					PERMEABILITY_DISTRIBUTION = "_het_k.dat",
					MASS_DISPERSION = "1 0.5 0.05"
)
# msh ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
					sim_basename = "2D1P_transport",
					filename = "2D1P_transport.msh",
					file_dir = "ogs5_benchmarks/C/hetk+n+restart")

# msp ----------------------------------------------------------------------
ogs5_obj <- input_add_msp_bloc(ogs5_obj, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003"
)
# num ----------------------------------------------------------------------
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2"
)
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS2",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT1",
					NOD_VALUES = "HEAD Tracer1",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 100"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT2",
					NOD_VALUES = "Tracer1",
					GEO_TYPE = "POINT POINT10",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT3",
					NOD_VALUES = "Tracer1",
					GEO_TYPE = "POINT POINT11",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW"
)
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
# st ----------------------------------------------------------------------
ogs5_obj <- input_add_st_bloc(ogs5_obj, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE R_BC",
					DIS_TYPE = "CONSTANT -1.15741E-05"
)
# tim ----------------------------------------------------------------------
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "100 21600",
					TIME_END = "2.0e10",
					TIME_START = "0.0"
)
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "100 21600",
					TIME_END = "2.0e10",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(ogs5_obj, "all")
# run ogs5 simulation----------------------------------------------------------------------
ogs5_run(ogs5_obj,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/C/hetk+n+restart/log")
