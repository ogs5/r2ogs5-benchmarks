lin <- create_ogs5(sim_name = "lin",
					sim_path = "r2ogs5_benchmarks/tmp/C/NAPL-dissolution/1D_NAPL-diss_trans/lin",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
lin <- input_add_bc_bloc(lin, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 98100"
)
lin <- input_add_bc_bloc(lin, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT 93195.00"
)
lin <- input_add_bc_bloc(lin, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 1.0"
)
# gli ----------------------------------------------------------------------
lin <- input_add_blocs_from_file(lin,
					sim_basename = "1D_TPF_resS_trans",
					filename = "1D_TPF_resS_trans.gli",
					file_dir = "ogs5_benchmarks/C/NAPL-dissolution/1D_NAPL-diss_trans/lin")

# ic ----------------------------------------------------------------------
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 98067"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "POLYLINE S_1",
					DIS_TYPE = "CONSTANT 0.899997748"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "POLYLINE S_2",
					DIS_TYPE = "CONSTANT 0.899997748"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION5",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION6",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "POLYLINE S_1",
					DIS_TYPE = "CONSTANT 0.100002252"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION7",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "POLYLINE S_2",
					DIS_TYPE = "CONSTANT 0.100002252"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION8",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
lin <- input_add_ic_bloc(lin, ic_name = "INITIAL_CONDITION9",
					PCS_TYPE = "MASS_TRANSPORT",
					PRIMARY_VARIABLE = "Tracer",
					GEO_TYPE = "POLYLINE LEFTPLINE",
					DIS_TYPE = "CONSTANT 1.0"
)
# mcp ----------------------------------------------------------------------
lin <- input_add_mcp_bloc(lin, NAME = "Tracer",
					MOBILE = "1",
					DIFFUSION = "1 1.0e-9"
)
# mfp ----------------------------------------------------------------------
lin <- input_add_mfp_bloc(lin, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1E+03",
					VISCOSITY = "1 1.307E-3",
					PHASE_DIFFUSION = "1 7.66E-10"
)
lin <- input_add_mfp_bloc(lin, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					DENSITY = "1 1E+03",
					VISCOSITY = "1 1.307E-3",
					PHASE_DIFFUSION = "1 7.66E-10"
)
# mmp ----------------------------------------------------------------------
lin <- input_add_mmp_bloc(lin, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "1",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 2.500000e-001",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.54249E-11",
					PERMEABILITY_SATURATION = "61 0.20 1.0 3.86 66 0.20 1.0 3.86 0e-9",
					CAPILLARY_PRESSURE = "6 0.0",
					MASS_DISPERSION = "1 0.5 1.0E-10",
)
# msh ----------------------------------------------------------------------
lin <- input_add_blocs_from_file(lin,
					sim_basename = "1D_TPF_resS_trans",
					filename = "1D_TPF_resS_trans.msh",
					file_dir = "ogs5_benchmarks/C/NAPL-dissolution/1D_NAPL-diss_trans/lin")

# msp ----------------------------------------------------------------------
lin <- input_add_msp_bloc(lin, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.65000e+003"
)
# num ----------------------------------------------------------------------
lin <- input_add_num_bloc(lin, num_name = "NUMERICS1",
					PCS_TYPE = "PS_GLOBAL",
					ELE_MASS_LUMPING = "1",
					ELE_UPWINDING = "0 1.0",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 1 1.e-12 2000 1.0 1 4",
					NON_LINEAR_SOLVER = "PICARD 1e-5 15 1.0"
)
lin <- input_add_num_bloc(lin, num_name = "NUMERICS2",
					PCS_TYPE = "MASS_TRANSPORT",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
lin <- input_add_out_bloc(lin, out_name = "OUTPUT1",
					NOD_VALUES = "Tracer",
					GEO_TYPE = "POINT POINT8",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
lin <- input_add_out_bloc(lin, out_name = "OUTPUT2",
					NOD_VALUES = "PRESSURE1 SATURATION1 SATURATION2",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "21600.0 43200.0 64800.0"
)
lin <- input_add_out_bloc(lin, out_name = "OUTPUT3",
					NOD_VALUES = "PRESSURE1 SATURATION1 SATURATION2 Tracer",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 50"
)
lin <- input_add_out_bloc(lin, out_name = "OUTPUT4",
					ELE_VALUES = "VELOCITY_X1 VELOCITY_Y1 VELOCITY_Z1 VELOCITY_X2 VELOCITY_Y2 VELOCITY_Z2",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "21600.0 43200.0 64800.0"
)
# pcs ----------------------------------------------------------------------
lin <- input_add_pcs_bloc(lin, pcs_name = "PROCESS1",
					PCS_TYPE = "PS_GLOBAL",
					NUM_TYPE = "dPcdSwGradSnw",
					ELEMENT_MATRIX_OUTPUT = "0"
)
lin <- input_add_pcs_bloc(lin, pcs_name = "PROCESS2",
					PCS_TYPE = "MASS_TRANSPORT",
					NUM_TYPE = "NEW"
)
# st ----------------------------------------------------------------------
lin <- input_add_st_bloc(lin, st_name = "SOURCE_TERM1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE RIGHTPLINE",
					DIS_TYPE = "CONSTANT 0.0"
)
# tim ----------------------------------------------------------------------
lin <- input_add_tim_bloc(lin, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "PS_GLOBAL",
					TIME_STEPS = "500 21600.0",
					TIME_END = "864000000.0",
					TIME_START = "0.0"
)
lin <- input_add_tim_bloc(lin, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "MASS_TRANSPORT",
					TIME_STEPS = "500 21600.0",
					TIME_END = "864000000.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(lin, "all")
# run ogs5 simulation----------------------------------------------------------------------
ogs5_run(lin, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/C/NAPL-dissolution/1D_NAPL-diss_trans/lin/log")
