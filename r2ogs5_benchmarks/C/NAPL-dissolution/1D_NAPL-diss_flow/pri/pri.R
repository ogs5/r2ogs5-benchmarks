pri <- create_ogs5(sim_name = "pri",
					sim_path = "r2ogs5_benchmarks/tmp/C/NAPL-dissolution/1D_NAPL-diss_flow/pri",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 98100"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT3",
					DIS_TYPE = "CONSTANT 98100"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT 93195.00"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT7",
					DIS_TYPE = "CONSTANT 93195.00"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 107910.00"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT2",
					DIS_TYPE = "CONSTANT 107910.00"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION7",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT5",
					DIS_TYPE = "CONSTANT 103005.00"
)
pri <- input_add_bc_bloc(pri, bc_name = "BOUNDARY_CONDITION8",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT6",
					DIS_TYPE = "CONSTANT 103005.00"
)
# gli ----------------------------------------------------------------------
pri <- input_add_blocs_from_file(pri,
					sim_basename = "1D_TPF_resS_flow",
					filename = "1D_TPF_resS_flow.gli",
					file_dir = "ogs5_benchmarks/C/NAPL-dissolution/1D_NAPL-diss_flow/pri")

# ic ----------------------------------------------------------------------
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 98067"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "POLYLINE S_1",
					DIS_TYPE = "CONSTANT 0.899997748"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION4",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION1",
					GEO_TYPE = "POLYLINE S_2",
					DIS_TYPE = "CONSTANT 0.899997748"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION5",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION6",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "POLYLINE S_1",
					DIS_TYPE = "CONSTANT 0.100002252"
)
pri <- input_add_ic_bloc(pri, ic_name = "INITIAL_CONDITION7",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "SATURATION2",
					GEO_TYPE = "POLYLINE S_2",
					DIS_TYPE = "CONSTANT 0.100002252"
)
# mfp ----------------------------------------------------------------------
pri <- input_add_mfp_bloc(pri, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 0.99970E+03",
					VISCOSITY = "1 1.307E-3",
					PHASE_DIFFUSION = "1 7.66E-10"
)
pri <- input_add_mfp_bloc(pri, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"SATURATION2",
					DENSITY = "1 0.99970E+03",
					VISCOSITY = "1 1.307E-3",
					PHASE_DIFFUSION = "1 7.66E-10"
)
# mmp ----------------------------------------------------------------------
pri <- input_add_mmp_bloc(pri, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 2.500000e-001",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.54249E-11",
					PERMEABILITY_SATURATION = "61 0.20 1.0 3.86 66 0.20 1.0 3.86 0e-9",
					CAPILLARY_PRESSURE = "6 0.0",
					MASS_DISPERSION = "1 0.5 1.0E-10",
					# DENSITY = ! is not a valid skey !"1 2650.0"
)
# msh ----------------------------------------------------------------------
pri <- input_add_blocs_from_file(pri,
					sim_basename = "1D_TPF_resS_flow",
					filename = "1D_TPF_resS_flow.msh",
					file_dir = "ogs5_benchmarks/C/NAPL-dissolution/1D_NAPL-diss_flow/pri")

# msp ----------------------------------------------------------------------
pri <- input_add_msp_bloc(pri, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.65000e+003"
)
# num ----------------------------------------------------------------------
pri <- input_add_num_bloc(pri, num_name = "NUMERICS1",
					PCS_TYPE = "PS_GLOBAL",
					ELE_MASS_LUMPING = "1",
					ELE_UPWINDING = "0 1.0",
					LINEAR_SOLVER = "2 1 1.e-12 2000 1.0 1 4",
					NON_LINEAR_SOLVER = "PICARD 1e-5 15 1.0"
)
# out ----------------------------------------------------------------------
pri <- input_add_out_bloc(pri, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 SATURATION1 SATURATION2",
					GEO_TYPE = "POLYLINE OUT_LINE",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
pri <- input_add_pcs_bloc(pri, pcs_name = "PROCESS1",
					PCS_TYPE = "PS_GLOBAL",
					NUM_TYPE = "dPcdSwGradSnw",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# st ----------------------------------------------------------------------
pri <- input_add_st_bloc(pri, st_name = "SOURCE_TERM1",
					PCS_TYPE = "PS_GLOBAL",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE RIGHTPLINE",
					DIS_TYPE = "CONSTANT 0.0"
)
# tim ----------------------------------------------------------------------
pri <- input_add_tim_bloc(pri, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "PS_GLOBAL",
					TIME_STEPS = "3 21600.0",
					TIME_END = "864000000.0",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(pri, "all")
# run ogs5 simulation----------------------------------------------------------------------
ogs5_run(pri,
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/C/NAPL-dissolution/1D_NAPL-diss_flow/pri/log")
