hm_dyn_tri <- create_ogs5(sim_name = "hm_dyn_tri",
					sim_path = "r2ogs5_benchmarks/tmp/HM/hm_dyn_tri",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
hm_dyn_tri <- input_add_bc_bloc(hm_dyn_tri, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE P_TOP",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_dyn_tri <- input_add_bc_bloc(hm_dyn_tri, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE UX_LEFT",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_dyn_tri <- input_add_bc_bloc(hm_dyn_tri, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE UX_RIGHT",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_dyn_tri <- input_add_bc_bloc(hm_dyn_tri, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE UX_BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_dyn_tri <- input_add_bc_bloc(hm_dyn_tri, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE UY_BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
# gli ----------------------------------------------------------------------
hm_dyn_tri <- input_add_blocs_from_file(hm_dyn_tri,
					sim_basename = "hm_dyn_tri",
					filename = "hm_dyn_tri.gli",
					file_dir = "ogs5_benchmarks/HM/hm_dyn_tri")

# ic ----------------------------------------------------------------------
hm_dyn_tri <- input_add_ic_bloc(hm_dyn_tri, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
hm_dyn_tri <- input_add_mfp_bloc(hm_dyn_tri, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1.0e0",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
hm_dyn_tri <- input_add_mmp_bloc(hm_dyn_tri, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.0",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.000000e-10"
)
# msh ----------------------------------------------------------------------
hm_dyn_tri <- input_add_blocs_from_file(hm_dyn_tri,
					sim_basename = "hm_dyn_tri",
					filename = "hm_dyn_tri.msh",
					file_dir = "ogs5_benchmarks/HM/hm_dyn_tri")

# msp ----------------------------------------------------------------------
hm_dyn_tri <- input_add_msp_bloc(hm_dyn_tri, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.0e0",
					ELASTICITY = "POISSION 2.0000e-001\n YOUNGS_MODULUS\n 1 3.e+4"
)
# num ----------------------------------------------------------------------
hm_dyn_tri <- input_add_num_bloc(hm_dyn_tri, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION",
					NON_LINEAR_SOLVER = "NEWTON 1e-2 1e-10 10 0.0",
					LINEAR_SOLVER = "2 5 1.e-12 5000 1.0 100 4",
					ELE_GAUSS_POINTS = "3",
					DYNAMIC_DAMPING = "0.515 0.51 0.51"
)
# out ----------------------------------------------------------------------
hm_dyn_tri <- input_add_out_bloc(hm_dyn_tri, out_name = "OUTPUT1",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE_RATE1 PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 ACCELERATION_X1 ACCELERATION_Y1 VELOCITY_DM_X VELOCITY_DM_Y STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
hm_dyn_tri <- input_add_out_bloc(hm_dyn_tri, out_name = "OUTPUT2",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 PRESSURE_RATE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 ACCELERATION_X1 ACCELERATION_Y1 VELOCITY_DM_X VELOCITY_DM_Y STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ",
					GEO_TYPE = "POLYLINE ex2_Out_axis",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1. 5. 10. 20."
)
hm_dyn_tri <- input_add_out_bloc(hm_dyn_tri, out_name = "OUTPUT3",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 PRESSURE_RATE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 ACCELERATION_X1 ACCELERATION_Y1 VELOCITY_DM_X VELOCITY_DM_Y STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ",
					GEO_TYPE = "POLYLINE ex2_out_Bedge",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "1. 5. 10. 20."
)
# pcs ----------------------------------------------------------------------
hm_dyn_tri <- input_add_pcs_bloc(hm_dyn_tri, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW DEFORMATION_DYNAMIC",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------

hm_dyn_tri <- input_add_rfd_bloc(hm_dyn_tri, rfd_name = "CURVES4",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										1296000.,
										2592000.),
								value = c(1.0,
										1.0,
										1.0))
)
# st ----------------------------------------------------------------------
hm_dyn_tri <- input_add_st_bloc(hm_dyn_tri, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE TRACTION_Y",
					DIS_TYPE = "LINEAR_NEUMANN 2 0 -1000.0 1 -1000.0"
)
# tim ----------------------------------------------------------------------
hm_dyn_tri <- input_add_tim_bloc(hm_dyn_tri, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION_FLOW",
					TIME_STEPS = "10 10.0",
					TIME_END = "1000.",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(hm_dyn_tri,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(hm_dyn_tri, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/HM/hm_dyn_tri/log")
