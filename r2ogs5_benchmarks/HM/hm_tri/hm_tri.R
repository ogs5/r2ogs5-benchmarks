tri <- create_ogs5(sim_name = "tri",
					sim_path = "r2ogs5_benchmarks/tmp/HM/hm_tri",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
tri <- input_add_bc_bloc(tri, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 0.0"
)
tri <- input_add_bc_bloc(tri, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE LEFT",
					DIS_TYPE = "CONSTANT 0.0"
)
tri <- input_add_bc_bloc(tri, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE RIGHT",
					DIS_TYPE = "CONSTANT 0.0"
)
tri <- input_add_bc_bloc(tri, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
# gli ----------------------------------------------------------------------
tri <- input_add_blocs_from_file(tri,
					sim_basename = "hm_tri",
					filename = "hm_tri.gli",
					file_dir = "ogs5_benchmarks/HM/hm_tri")

# ic ----------------------------------------------------------------------
tri <- input_add_ic_bloc(tri, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
tri <- input_add_mfp_bloc(tri, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.0",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
tri <- input_add_mmp_bloc(tri, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.000000e-001",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.000000e-10"
)
# msh ----------------------------------------------------------------------
tri <- input_add_blocs_from_file(tri,
					sim_basename = "hm_tri",
					filename = "hm_tri.msh",
					file_dir = "ogs5_benchmarks/HM/hm_tri")

# msp ----------------------------------------------------------------------
tri <- input_add_msp_bloc(tri, NAME = "SOLID_PROPERTIES1",
					ELASTICITY = "POISSION: 2.0000e-001\n YOUNGS_MODULUS\n 1 3.e+4",
					PLASTICITY = "DRUCKER-PRAGER\n 1.0e16\n -1.0e+6\n 19.0\n 2.0\n 0.0"
)
# num ----------------------------------------------------------------------
tri <- input_add_num_bloc(tri, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION_FLOW",
					LINEAR_SOLVER = "2 5 1.e-009 10000 1.0 100 4",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
tri <- input_add_out_bloc(tri, out_name = "OUTPUT1",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
tri <- input_add_out_bloc(tri, out_name = "OUTPUT2",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE RIGHT",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
tri <- input_add_out_bloc(tri, out_name = "OUTPUT3",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1",
					GEO_TYPE = "POINT POINT0",
					DAT_TYPE = "TECPLOT"
)
# pcs ----------------------------------------------------------------------
tri <- input_add_pcs_bloc(tri, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW DEFORMATION"
)
# rfd ----------------------------------------------------------------------
tri <- input_add_rfd_bloc(tri, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										10.),
								value = c(0.0,
										0.0))
)
# st ----------------------------------------------------------------------
tri <- input_add_st_bloc(tri, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "LINEAR_NEUMANN 2 0 -1000.0 1 -1000.0"
)
# tim ----------------------------------------------------------------------
tri <- input_add_tim_bloc(tri, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION_FLOW",
					TIME_STEPS = "10 1.",
					TIME_END = "10.",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(tri,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(tri, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/HM/hm_tri/log")
