hm_cc_tri_s <- create_ogs5(sim_name = "hm_cc_tri_s",
					sim_path = "r2ogs5_benchmarks/tmp/HM/hm_cc_tri_s",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_bc_bloc(hm_cc_tri_s, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_cc_tri_s <- input_add_bc_bloc(hm_cc_tri_s, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE LEFT",
					DIS_TYPE = "CONSTANT 0.0"
)
hm_cc_tri_s <- input_add_bc_bloc(hm_cc_tri_s, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT -1.0",
					TIM_TYPE = "CURVE 2"
)
hm_cc_tri_s <- input_add_bc_bloc(hm_cc_tri_s, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
# gli ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_blocs_from_file(hm_cc_tri_s,
					sim_basename = "hm_cc_tri_s",
					filename = "hm_cc_tri_s.gli",
					file_dir = "ogs5_benchmarks/HM/hm_cc_tri_s")

# ic ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_ic_bloc(hm_cc_tri_s, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "DEFORMATION_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mfp ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_mfp_bloc(hm_cc_tri_s, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.0",
					VISCOSITY = "1 1.000000e-003"
)
# mmp ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_mmp_bloc(hm_cc_tri_s, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.2",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0e-11"
)
# msh ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_blocs_from_file(hm_cc_tri_s,
					sim_basename = "hm_cc_tri_s",
					filename = "hm_cc_tri_s.msh",
					file_dir = "ogs5_benchmarks/HM/hm_cc_tri_s")

# msp ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_msp_bloc(hm_cc_tri_s, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 0.0",
					ELASTICITY = "POISSION 0.3",
					PLASTICITY = "CAM-CLAY 1.2 0.2 0.02 60.0 1.5 1.0  -50.0  -50.0  -50.0  0.0"
)
# num ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_num_bloc(hm_cc_tri_s, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION_FLOW",
					NON_LINEAR_SOLVER = "NEWTON 1e-4 2e-10 100 0.0",
					LINEAR_SOLVER = "2 5 1.e-010 2000 1.0 100 4",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_out_bloc(hm_cc_tri_s, out_name = "OUTPUT1",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_PLS",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
hm_cc_tri_s <- input_add_out_bloc(hm_cc_tri_s, out_name = "OUTPUT2",
					PCS_TYPE = "DEFORMATION_FLOW",
					NOD_VALUES = "PRESSURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT2",
					DAT_TYPE = "TECPLOT"
)
# pcs ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_pcs_bloc(hm_cc_tri_s, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW DEFORMATION",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------

hm_cc_tri_s <- input_add_rfd_bloc(hm_cc_tri_s, rfd_name = "CURVES2",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0,
										11.0e8),
								value = c(-50.0,
										-200.0))
)
hm_cc_tri_s <- input_add_rfd_bloc(hm_cc_tri_s, rfd_name = "CURVES3",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0,
										1.1e8),
								value = c(0.0,
										0.6))
)
# tim ----------------------------------------------------------------------
hm_cc_tri_s <- input_add_tim_bloc(hm_cc_tri_s, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION_FLOW",
					TIME_STEPS = "50 2.e6",
					TIME_END = "1e8",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(hm_cc_tri_s,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(hm_cc_tri_s, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/HM/hm_cc_tri_s/log")
