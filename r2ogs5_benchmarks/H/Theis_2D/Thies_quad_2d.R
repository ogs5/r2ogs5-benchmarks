# Benchmark: Theis_2D

# define ogs5 obj ---------------------------------------------------------
theis_2D <- create_ogs5(sim_name = "theis_2D",
					sim_path = "r2ogs5_benchmarks/tmp/H/Theis_2D",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
theis_2D <- input_add_bc_bloc(theis_2D, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE left_bc",
					DIS_TYPE = "CONSTANT 20.0")

theis_2D <- input_add_bc_bloc(theis_2D, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POLYLINE right_bc",
					DIS_TYPE = "CONSTANT 20.0")

# gli ----------------------------------------------------------------------
theis_2D <- input_add_blocs_from_file(theis_2D,
                    sim_basename = "Thies_quad_2d",
					filename = "Thies_quad_2d.gli",
					file_dir = "ogs5_benchmarks/H/Theis_2D")

# ic ----------------------------------------------------------------------
theis_2D <- input_add_ic_bloc(theis_2D, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 20.0")

# mfp ----------------------------------------------------------------------
theis_2D <- input_add_mfp_bloc(theis_2D, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 0.000000e+0",
					VISCOSITY = "1 1.0")

# mmp ----------------------------------------------------------------------
theis_2D <- input_add_mmp_bloc(theis_2D, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "20.000000e+000",
					STORAGE = "1 0.00001",
					PERMEABILITY_SATURATION = "1 1.0",
					PERMEABILITY_TENSOR = "ISOTROPIC 5.787037037037e-4")

# msh ----------------------------------------------------------------------
theis_2D <- input_add_blocs_from_file(theis_2D,
					filename = "Thies_quad_2d.msh",
					sim_basename = "Thies_quad_2d",
					file_dir = "ogs5_benchmarks/H/Theis_2D")

# num ----------------------------------------------------------------------
theis_2D <- input_add_num_bloc(theis_2D, num_name = "NUMERICS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					LINEAR_SOLVER = "2 5 1.e-014 1000 1.0 100 4",
					RENUMBER = "2 -1")

# out ----------------------------------------------------------------------
theis_2D <- input_add_out_bloc(theis_2D, out_name = "OUTPUT1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1")

theis_2D <- input_add_out_bloc(theis_2D, out_name = "OUTPUT2",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NOD_VALUES = "HEAD",
					GEO_TYPE = "POINT POINT5",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1")

# pcs ----------------------------------------------------------------------
theis_2D <- input_add_pcs_bloc(theis_2D, pcs_name = "PROCESS1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					NUM_TYPE = "NEW",
					PRIMARY_VARIABLE = "HEAD")

# st ----------------------------------------------------------------------
theis_2D <- input_add_st_bloc(theis_2D, st_name = "SOURCE_TERM1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					PRIMARY_VARIABLE = "HEAD",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT -1000")

# tim ----------------------------------------------------------------------
theis_2D <- input_add_tim_bloc(theis_2D, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "GROUNDWATER_FLOW",
					TIME_STEPS = "146 1.2e-5",
					TIME_END = "0.00175",
					TIME_START = "0.0",
					TIME_UNIT = "DAY")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(theis_2D, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = theis_2D, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H/Theis_2D/log")
