m_e_transiso_3D <- create_ogs5(sim_name = "m_e_transiso_3D",
					sim_path = "r2ogs5_benchmarks/tmp/M/elasticity/m_e_transiso_3D",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURF_BOT",
					DIS_TYPE = "CONSTANT 0"
)
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Z1",
					GEO_TYPE = "SURFACE SURF_TOP",
					DIS_TYPE = "CONSTANT 0"
)
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURF_LEFT",
					DIS_TYPE = "CONSTANT 0"
)
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0"
)
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POINT POINT4",
					DIS_TYPE = "CONSTANT 0"
)
m_e_transiso_3D <- input_add_bc_bloc(m_e_transiso_3D, bc_name = "BOUNDARY_CONDITION6",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POINT POINT8",
					DIS_TYPE = "CONSTANT 0"
)
# gli ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_blocs_from_file(m_e_transiso_3D,
					sim_basename = "m_e_transiso_3D",
					filename = "m_e_transiso_3D.gli",
					file_dir = "ogs5_benchmarks/M/elasticity/m_e_transiso_3D")

# mmp ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_mmp_bloc(m_e_transiso_3D, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 0.000000e-001"
)
# msh ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_blocs_from_file(m_e_transiso_3D,
					sim_basename = "m_e_transiso_3D",
					filename = "m_e_transiso_3D.msh",
					file_dir = "ogs5_benchmarks/M/elasticity/m_e_transiso_3D")

# msp ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_msp_bloc(m_e_transiso_3D, NAME = "SOLID_PROPERTIES1",
					ELASTICITY = "POISSION 0.6032\n YOUNGS_MODULUS\n 10 561.121e6 1311.83e6 0.18384 375.0e6 -0.994522 0.10453 0.0"
)
# num ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_num_bloc(m_e_transiso_3D, num_name = "NUMERICS1",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 5 1.e-16 10000 1.0 100 4",
					ELE_GAUSS_POINTS = "3"
)
# out ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT1",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT2",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "VTK",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT3",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT1",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT4",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT5",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT5",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT2",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT6",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT6",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT7",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT3",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
m_e_transiso_3D <- input_add_out_bloc(m_e_transiso_3D, out_name = "OUTPUT8",
					NOD_VALUES = "DISPLACEMENT_X1 DISPLACEMENT_Y1 DISPLACEMENT_Z1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRESS_XZ STRESS_YZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_ZZ STRAIN_XZ STRAIN_YZ STRAIN_PLS",
					GEO_TYPE = "POINT POINT7",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_pcs_bloc(m_e_transiso_3D, pcs_name = "PROCESS1",
					PCS_TYPE = "DEFORMATION"
)
# st ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_st_bloc(m_e_transiso_3D, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "SURFACE SURF_RIGHT",
					DIS_TYPE = "CONSTANT_NEUMANN 0.2e6"
)
# tim ----------------------------------------------------------------------
m_e_transiso_3D <- input_add_tim_bloc(m_e_transiso_3D, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "DEFORMATION",
					TIME_STEPS = "1 10.",
					TIME_END = "600.",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(m_e_transiso_3D,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(m_e_transiso_3D, ogs_exe = "ogs_fem",
					run_path = NULL,
					log_output = TRUE,
					log_path = "r2ogs5_benchmarks/tmp/M/elasticity/m_e_transiso_3D/log")
