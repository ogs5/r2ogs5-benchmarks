# Benchmark: h_us_drainage

# define ogs5 obj ---------------------------------------------------------
h_us_drainage <- create_ogs5(sim_name = "h_us_drainage",
					sim_path = "r2ogs5_benchmarks/tmp/H_us/Drainage",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
h_us_drainage <- input_add_bc_bloc(h_us_drainage, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 0.0")

# gli ----------------------------------------------------------------------
h_us_drainage <- input_add_blocs_from_file(h_us_drainage,
                    sim_basename = "h_us_drainage",
					filename = "h_us_drainage.gli",
					file_dir = "ogs5_benchmarks/H_us/Drainage")

# ic ----------------------------------------------------------------------
h_us_drainage <- input_add_ic_bloc(h_us_drainage, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0")

# mfp ----------------------------------------------------------------------
h_us_drainage <- input_add_mfp_bloc(h_us_drainage, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

# mmp ----------------------------------------------------------------------
h_us_drainage <- input_add_mmp_bloc(h_us_drainage, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.2975",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.5e-13",
					PERMEABILITY_SATURATION = "0 2",
					CAPILLARY_PRESSURE = "0 1")

# msh ----------------------------------------------------------------------
h_us_drainage <- input_add_blocs_from_file(h_us_drainage,
					filename = "h_us_drainage.msh",
					sim_basename = "h_us_drainage",
					file_dir = "ogs5_benchmarks/H_us/Drainage")

# num ----------------------------------------------------------------------
h_us_drainage <- input_add_num_bloc(h_us_drainage, num_name = "NUMERICS1",
					PCS_TYPE = "RICHARDS_FLOW",
					ELE_UPWINDING = "0.5",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "3 6 1.e-010 1000 1.0 101 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-3 10 0.0")

# out ----------------------------------------------------------------------
h_us_drainage <- input_add_out_bloc(h_us_drainage, out_name = "OUTPUT1",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "DOMAIN",
					TIM_TYPE = "180 600 1800 3600 5400 7200 10800 18000 36000 72000 75600 79200 82800 86400",
					DAT_TYPE = "TECPLOT")

# pcs ----------------------------------------------------------------------
h_us_drainage <- input_add_pcs_bloc(h_us_drainage, pcs_name = "PROCESS1",
					PCS_TYPE = "RICHARDS_FLOW",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
h_us_drainage <- input_add_blocs_from_file(h_us_drainage,
                    filename = "h_us_drainage.rfd",
                    sim_basename = "h_us_drainage",
                    file_dir = "ogs5_benchmarks/H_us/Drainage")

# st ----------------------------------------------------------------------
h_us_drainage <- input_add_st_bloc(h_us_drainage, st_name = "SOURCE_TERM1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 0.0")

# tim ----------------------------------------------------------------------
h_us_drainage <- input_add_tim_bloc(h_us_drainage, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "RICHARDS_FLOW",
					TIME_UNIT = "SECOND",
					TIME_END = "86400",
					TIME_START = "0.0",
					TIME_STEPS = "10000 10 10000 10 35000 10 100000 10")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(h_us_drainage, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = h_us_drainage, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H_us/Drainage/log")
