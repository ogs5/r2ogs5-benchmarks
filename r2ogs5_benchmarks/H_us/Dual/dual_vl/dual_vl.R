# Benchmark: dual_vl

# define ogs5 obj ---------------------------------------------------------
dual_vl <- create_ogs5(sim_name = "dual_vl",
					sim_path = "r2ogs5_benchmarks/tmp/H_us/Dual/dual_vl",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
dual_vl <- input_add_bc_bloc(dual_vl, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 98")

dual_vl <- input_add_bc_bloc(dual_vl, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 98")

# gli ----------------------------------------------------------------------
dual_vl <- input_add_blocs_from_file(dual_vl,
                    sim_basename = "dual_vl",
					filename = "dual_vl.gli",
					file_dir = "ogs5_benchmarks/H_us/Dual/dual_vl")

# ic ----------------------------------------------------------------------
dual_vl <- input_add_ic_bloc(dual_vl, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "GRADIENT 0.6 -27440 9800")

dual_vl <- input_add_ic_bloc(dual_vl, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "GRADIENT 0.6 -27440 9800")

# mfp ----------------------------------------------------------------------
dual_vl <- input_add_mfp_bloc(dual_vl, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

# mmp ----------------------------------------------------------------------
dual_vl <- input_add_mmp_bloc(dual_vl, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.498",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.32368E-13",
					PERMEABILITY_SATURATION = "4 0 1. 0.444444444",
					CAPILLARY_PRESSURE = "4 1.8",
					TRANSFER_COEFFICIENT = "500.0")

dual_vl <- input_add_mmp_bloc(dual_vl, NAME = "MEDIUM_PROPERTIES2",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.5",
					TORTUOSITY = "1 0.8",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.09000E-11",
					PERMEABILITY_SATURATION = "4 0 1. 0.444444444",
					CAPILLARY_PRESSURE = "4 5.6",
					TRANSFER_COEFFICIENT = "500.0")

# msh ----------------------------------------------------------------------
dual_vl <- input_add_blocs_from_file(dual_vl,
					filename = "dual_vl.msh",
					sim_basename = "dual_vl",
					file_dir = "ogs5_benchmarks/H_us/Dual/dual_vl")

# msp ----------------------------------------------------------------------
dual_vl <- input_add_msp_bloc(dual_vl, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003",
					THERMAL = "EXPANSION: 1.0e-5 CAPACITY: 1 1605.064889 CONDUCTIVITY: 1 0.00118799939")

dual_vl <- input_add_msp_bloc(dual_vl, NAME = "SOLID_PROPERTIES2",
					DENSITY = "1 2.00000e+003",
					THERMAL = "EXPANSION: 1.0e-5 CAPACITY: 1 1605.064889 CONDUCTIVITY: 1 0.00118799939")

# num ----------------------------------------------------------------------
dual_vl <- input_add_num_bloc(dual_vl, num_name = "NUMERICS1",
					PCS_TYPE = "RICHARDS_FLOW",
					ELE_UPWINDING = "0.5",
					ELE_MASS_LUMPING = "1",
					LINEAR_SOLVER = "3 6 1.e-010 1000 1.0 101 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-3 50 0.0",
					ELE_GAUSS_POINTS = "3")

# out ----------------------------------------------------------------------
dual_vl <- input_add_out_bloc(dual_vl, out_name = "OUTPUT1",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 PRESSURE2 SATURATION1 SATURATION2 TOTAL_SATURATION",
					GEO_TYPE = "DOMAIN",
					TIM_TYPE = "1. 600. 1200. 1800. 32400. 61200. 73800. 77400. 79200. 81000. 86400. 93600.",
					DAT_TYPE = "TECPLOT")

# pcs ----------------------------------------------------------------------
dual_vl <- input_add_pcs_bloc(dual_vl, pcs_name = "PROCESS1",
					PCS_TYPE = "RICHARDS_FLOW",
					NUM_TYPE = "NEW",
					MEDIUM_TYPE = "CONTINUUM 0.95")

# rfd ----------------------------------------------------------------------

dual_vl <- input_add_rfd_bloc(dual_vl, rfd_name = "CURVES2",
                               mkey = "CURVES",
                               data = tibble::tibble(
                                 time = c(0.26315789,
                                          0.39473684,
                                          0.52631579,
                                          0.57894737,
                                          0.63157895,
                                          0.65789474,
                                          0.78947368,
                                          0.81578947,
                                          0.84210526,
                                          0.86842105,
                                          0.89473684,
                                          0.92105263,
                                          0.94894737,
                                          0.96052632,
                                          0.97368421,
                                          0.98684211,
                                          1.00000000),
                                 value = c(4.4323E-05,
                                           0.00026547,
                                           0.00159003,
                                           0.00325358,
                                           0.00665757,
                                           0.00952343,
                                           0.05704014,
                                           0.08159396,
                                           0.11671736,
                                           0.16696017,
                                           0.23883078,
                                           0.34163922,
                                           0.49931406,
                                           0.58449912,
                                           0.69907308,
                                           0.8361059,
                                           1.0000000))
)
dual_vl <- input_add_rfd_bloc(dual_vl, rfd_name = "CURVES3",
                               mkey = "CURVES",
                               data = tibble::tibble(
                                 time = c(0.26315789,
                                          0.39473684,
                                          0.52631579,
                                          0.57894737,
                                          0.63157895,
                                          0.65789474,
                                          0.78947368,
                                          0.81578947,
                                          0.84210526,
                                          0.86842105,
                                          0.89473684,
                                          0.92105263,
                                          0.94894737,
                                          0.96052632,
                                          0.97368421,
                                          0.98684211,
                                          1.00000000),
                                 value = c(44627.5611,
                                           26399.6732,
                                           15616.8683,
                                           12658.7874,
                                           10261.0136,
                                           9238.24221,
                                           5464.93174,
                                           4920.21206,
                                           4429.78757,
                                           3988.24638,
                                           3590.71602,
                                           3232.80969,
                                           2892.29884,
                                           2462.21599,
                                           2050.45507,
                                           1707.55369,
                                           1421.99634))
)
dual_vl <- input_add_rfd_bloc(dual_vl, rfd_name = "CURVES4",
                               mkey = "CURVES",
                               data = tibble::tibble(
                                 time = c(0.0,
                                          300.0,
                                          301.0,
                                          10080.0,
                                          10081.0,
                                          1.0e+11),
                                 value = c(1.0,
                                           1.0,
                                           1.0,
                                           1.0,
                                           0.0,
                                           0.0))
)
# tim ----------------------------------------------------------------------
dual_vl <- input_add_tim_bloc(dual_vl, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "RICHARDS_FLOW",
					TIME_STEPS = "1000 1 1000 1 100 1e+1 1000 1e+2",
					TIME_END = "1900.",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(dual_vl, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = dual_vl, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H_us/Dual/dual_vl/log")
