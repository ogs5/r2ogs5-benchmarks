# Benchmark: AT_5

# define ogs5 obj ---------------------------------------------------------
AT_5 <- create_ogs5(sim_name = "AT_5",
					sim_path = "r2ogs5_benchmarks/tmp/H_us/RSM",
					sim_id = 1L)

# fct ----------------------------------------------------------------------
AT_5 <- input_add_blocs_from_file(AT_5,
                    sim_basename = "AT_5",
                    filename = "AT_5.fct",
                    file_dir = "ogs5_benchmarks/H_us/RSM")


# gli ----------------------------------------------------------------------
AT_5 <- input_add_blocs_from_file(AT_5,
                    sim_basename = "AT_5",
					filename = "AT_5.gli",
					file_dir = "ogs5_benchmarks/H_us/RSM")

# ic ----------------------------------------------------------------------
AT_5 <- input_add_ic_bloc(AT_5, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT -90000.0")

# mfp ----------------------------------------------------------------------
AT_5 <- input_add_mfp_bloc(AT_5, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"HEAD",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

# mmp ----------------------------------------------------------------------
AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B01",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.43",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.0599E-13",
					PERMEABILITY_SATURATION = "0 2",
					CAPILLARY_PRESSURE = "0 1")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B02",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.43",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.1385E-13",
					PERMEABILITY_SATURATION = "0 4",
					CAPILLARY_PRESSURE = "0 3")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B03",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.45",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.1012E-13",
					PERMEABILITY_SATURATION = "0 6",
					CAPILLARY_PRESSURE = "0 5")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B04",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.42",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.4654E-13",
					PERMEABILITY_SATURATION = "0 8",
					CAPILLARY_PRESSURE = "0 7")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B06",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.39",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.3722E-13",
					PERMEABILITY_SATURATION = "0 10",
					CAPILLARY_PRESSURE = "0 9")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B07",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.40",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.6600E-13",
					PERMEABILITY_SATURATION = "0 12",
					CAPILLARY_PRESSURE = "0 11")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B08",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.43",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.6546E-14",
					PERMEABILITY_SATURATION = "0 14",
					CAPILLARY_PRESSURE = "0 13")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B09",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.43",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.8169E-14",
					PERMEABILITY_SATURATION = "0 16",
					CAPILLARY_PRESSURE = "0 15")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B10",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.42",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.3804E-14",
					PERMEABILITY_SATURATION = "0 18",
					CAPILLARY_PRESSURE = "0 17")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B11",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.60",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.2059E-14",
					PERMEABILITY_SATURATION = "0 20",
					CAPILLARY_PRESSURE = "0 19")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B12",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.55",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.8240E-13",
					PERMEABILITY_SATURATION = "0 22",
					CAPILLARY_PRESSURE = "0 21")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B14",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.42",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 9.4386E-15",
					PERMEABILITY_SATURATION = "0 24",
					CAPILLARY_PRESSURE = "0 23")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B15",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.62",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.4806E-13",
					PERMEABILITY_SATURATION = "0 26",
					CAPILLARY_PRESSURE = "0 25")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B16",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.73",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.5857E-13",
					PERMEABILITY_SATURATION = "0 28",
					CAPILLARY_PRESSURE = "0 27")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B17",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.72",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 5.2620E-14",
					PERMEABILITY_SATURATION = "0 30",
					CAPILLARY_PRESSURE = "0 29")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "B18",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.77",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 7.8694E-14",
					PERMEABILITY_SATURATION = "0 32",
					CAPILLARY_PRESSURE = "0 31")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O01",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.36",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.5585E-13",
					PERMEABILITY_SATURATION = "0 34",
					CAPILLARY_PRESSURE = "0 33")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O02",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.38",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.8358E-13",
					PERMEABILITY_SATURATION = "0 36",
					CAPILLARY_PRESSURE = "0 35")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O03",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.34",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.1591E-13",
					PERMEABILITY_SATURATION = "0 38",
					CAPILLARY_PRESSURE = "0 37")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O04",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.36",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.2648E-13",
					PERMEABILITY_SATURATION = "0 40",
					CAPILLARY_PRESSURE = "0 39")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O05",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.32",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 5.1381E-13",
					PERMEABILITY_SATURATION = "0 42",
					CAPILLARY_PRESSURE = "0 41")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O06",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.41",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.4654E-14",
					PERMEABILITY_SATURATION = "0 44",
					CAPILLARY_PRESSURE = "0 43")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O08",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.47",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0713E-13",
					PERMEABILITY_SATURATION = "0 46",
					CAPILLARY_PRESSURE = "0 45")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O09",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.46",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.6310E-14",
					PERMEABILITY_SATURATION = "0 48",
					CAPILLARY_PRESSURE = "0 47")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O10",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.49",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.6192E-14",
					PERMEABILITY_SATURATION = "0 50",
					CAPILLARY_PRESSURE = "0 49")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O11",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.42",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.6270E-13",
					PERMEABILITY_SATURATION = "0 52",
					CAPILLARY_PRESSURE = "0 51")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O12",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.56",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.3450E-14",
					PERMEABILITY_SATURATION = "0 54",
					CAPILLARY_PRESSURE = "0 53")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O13",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.57",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.9170E-14",
					PERMEABILITY_SATURATION = "0 56",
					CAPILLARY_PRESSURE = "0 55")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O14",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.38",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.2474E-15",
					PERMEABILITY_SATURATION = "0 58",
					CAPILLARY_PRESSURE = "0 57")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O15",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.41",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.3653E-14",
					PERMEABILITY_SATURATION = "0 60",
					CAPILLARY_PRESSURE = "0 59")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O16",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.89",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.2624E-14",
					PERMEABILITY_SATURATION = "0 62",
					CAPILLARY_PRESSURE = "0 61")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O17",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.86",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.2445E-14",
					PERMEABILITY_SATURATION = "0 64",
					CAPILLARY_PRESSURE = "0 63")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "O18",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.73",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.8771E-13",
					PERMEABILITY_SATURATION = "0 66",
					CAPILLARY_PRESSURE = "0 65")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H01",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.35",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.1855E-13",
					PERMEABILITY_SATURATION = "0 68",
					CAPILLARY_PRESSURE = "0 67")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H02",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.29",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.1798E-12",
					PERMEABILITY_SATURATION = "0 70",
					CAPILLARY_PRESSURE = "0 69")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H03",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.29",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.3006E-12",
					PERMEABILITY_SATURATION = "0 72",
					CAPILLARY_PRESSURE = "0 71")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H04",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.36",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 9.4386E-14",
					PERMEABILITY_SATURATION = "0 74",
					CAPILLARY_PRESSURE = "0 73")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H05",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.38",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 7.0789E-13",
					PERMEABILITY_SATURATION = "0 76",
					CAPILLARY_PRESSURE = "0 75")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H06",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.38",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.2388E-12",
					PERMEABILITY_SATURATION = "0 78",
					CAPILLARY_PRESSURE = "0 77")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H07",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.48",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.1798E-13",
					PERMEABILITY_SATURATION = "0 80",
					CAPILLARY_PRESSURE = "0 79")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H08",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.30",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 9.4386E-12",
					PERMEABILITY_SATURATION = "0 82",
					CAPILLARY_PRESSURE = "0 81")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H09",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.38",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.5394E-12",
					PERMEABILITY_SATURATION = "0 84",
					CAPILLARY_PRESSURE = "0 83")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H10",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.50",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.1236E-14",
					PERMEABILITY_SATURATION = "0 86",
					CAPILLARY_PRESSURE = "0 85")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H11",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.32",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.7697E-12",
					PERMEABILITY_SATURATION = "0 88",
					CAPILLARY_PRESSURE = "0 87")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H12",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.30",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.4890E-13",
					PERMEABILITY_SATURATION = "0 90",
					CAPILLARY_PRESSURE = "0 89")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H13",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.87",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.4777E-13",
					PERMEABILITY_SATURATION = "0 92",
					CAPILLARY_PRESSURE = "0 91")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H14",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.68",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.7193E-14",
					PERMEABILITY_SATURATION = "0 94",
					CAPILLARY_PRESSURE = "0 93")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H15",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.34",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.3035E-12",
					PERMEABILITY_SATURATION = "0 96",
					CAPILLARY_PRESSURE = "0 95")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H16",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.44",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.7697E-12",
					PERMEABILITY_SATURATION = "0 98",
					CAPILLARY_PRESSURE = "0 97")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H17",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.47",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.1294E-14",
					PERMEABILITY_SATURATION = "0 100",
					CAPILLARY_PRESSURE = "0 99")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H18",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.30",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 6.4890E-14",
					PERMEABILITY_SATURATION = "0 102",
					CAPILLARY_PRESSURE = "0 101")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H19",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.31",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 8.2588E-13",
					PERMEABILITY_SATURATION = "0 104",
					CAPILLARY_PRESSURE = "0 103")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H20",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.34",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.5956E-12",
					PERMEABILITY_SATURATION = "0 106",
					CAPILLARY_PRESSURE = "0 105")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H21",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.32",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 3.7754E-12",
					PERMEABILITY_SATURATION = "0 108",
					CAPILLARY_PRESSURE = "0 107")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H22",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.91",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 9.4386E-14",
					PERMEABILITY_SATURATION = "0 110",
					CAPILLARY_PRESSURE = "0 109")

AT_5 <- input_add_mmp_bloc(AT_5, NAME = "H23",
					GEO_TYPE = "DOMAIN",
					GEOMETRY_DIMENSION = "3",
					GEOMETRY_AREA = "1.000000000000e+000",
					POROSITY = "1 0.32",
					STORAGE = "1 1e-10",
					TORTUOSITY = "1 1.0000E+00",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.1294E-13",
					PERMEABILITY_SATURATION = "0 112",
					CAPILLARY_PRESSURE = "0 111")

# msh ----------------------------------------------------------------------
AT_5 <- input_add_blocs_from_file(AT_5,
                    sim_basename = "AT_5",
					filename = "AT_5.msh",
					file_dir = "ogs5_benchmarks/H_us/RSM")

# num ----------------------------------------------------------------------
AT_5 <- input_add_num_bloc(AT_5, num_name = "NUMERICS1",
					PCS_TYPE = "RICHARDS_FLOW",
					ELE_MASS_LUMPING = "1",
					ELE_GAUSS_POINTS = "2",
					LINEAR_SOLVER = "2 0 1.0e-020 2000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-05 100 0.0")

# out ----------------------------------------------------------------------
AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT1",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "LAYER",
					TIM_TYPE = "STEPS 1000",
					DAT_TYPE = "TECPLOT")

AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT2",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT1",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1 5 10 20 30 40 50 60 70 80 100 120 150 200 250 300 365")

AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT3",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT2",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1 5 10 20 30 40 50 60 70 80 100 120 150 200 250 300 365")

AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT4",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT3",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1 5 10 20 30 40 50 60 70 80 100 120 150 200 250 300 365")

AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT5",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT4",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1 5 10 20 30 40 50 60 70 80 100 120 150 200 250 300 365")

AT_5 <- input_add_out_bloc(AT_5, out_name = "OUTPUT6",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT5",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.5 1 5 10 20 30 40 50 60 70 80 100 120 150 200 250 300 365")

# pcs ----------------------------------------------------------------------
AT_5 <- input_add_pcs_bloc(AT_5, pcs_name = "PROCESS1",
					PCS_TYPE = "RICHARDS_FLOW",
					NUM_TYPE = "NEW")

AT_5 <- input_add_blocs_from_file(AT_5,
                    sim_basename = "AT_5",
                    filename = "AT_5.rfd",
                    file_dir = "ogs5_benchmarks/H_us/RSM")

# st ----------------------------------------------------------------------
AT_5 <- input_add_st_bloc(AT_5, st_name = "SOURCE_TERM1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT5412",
					DIS_TYPE = "CONSTANT 1.0",
					FCT_TYPE = "ST_FUNCTION")

# tim ----------------------------------------------------------------------
AT_5 <- input_add_tim_bloc(AT_5, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "RICHARDS_FLOW",
					TIME_UNIT = "DAY",
					TIME_END = "365",
					TIME_START = "0.0",
					TIME_CONTROL = "PI_AUTO_STEP_SIZE\n 1 1.0e-3 1.0e-9 1.7")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(AT_5, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = AT_5, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H_us/RSM/log")
