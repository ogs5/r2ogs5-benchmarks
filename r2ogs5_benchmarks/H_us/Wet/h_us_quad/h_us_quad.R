# Benchmark: h_us_quad

# define ogs5 obj ---------------------------------------------------------
h_us_quad <- create_ogs5(sim_name = "h_us_quad",
					sim_path = "r2ogs5_benchmarks/tmp/H_us/Wet/h_us_quad",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
h_us_quad <- input_add_bc_bloc(h_us_quad, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT -21500.")

h_us_quad <- input_add_bc_bloc(h_us_quad, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 0.0")

# gli ----------------------------------------------------------------------
h_us_quad <- input_add_blocs_from_file(h_us_quad,
                    sim_basename = "h_us_quad",
					filename = "h_us_quad.gli",
					file_dir = "ogs5_benchmarks/H_us/Wet/h_us_quad")

# ic ----------------------------------------------------------------------
h_us_quad <- input_add_ic_bloc(h_us_quad, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT -21500")

# mfp ----------------------------------------------------------------------
h_us_quad <- input_add_mfp_bloc(h_us_quad, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003")

# mmp ----------------------------------------------------------------------
h_us_quad <- input_add_mmp_bloc(h_us_quad, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.0",
					POROSITY = "1 0.38",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 4.46e-13",
					PERMEABILITY_SATURATION = "0 2 0 2",
					CAPILLARY_PRESSURE = "0 1")

# msh ----------------------------------------------------------------------
h_us_quad <- input_add_blocs_from_file(h_us_quad,
                    sim_basename = "h_us_quad",
					filename = "h_us_quad.msh",
					file_dir = "ogs5_benchmarks/H_us/Wet/h_us_quad")

# num ----------------------------------------------------------------------
h_us_quad <- input_add_num_bloc(h_us_quad, num_name = "NUMERICS1",
					PCS_TYPE = "RICHARDS_FLOW",
					ELE_UPWINDING = "0.5",
					ELE_MASS_LUMPING = "1",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "3 6 1.e-010 1000 1.0 101 4",
					NON_LINEAR_SOLVER = "PICARD 1.0e-3 50 0.0")

# out ----------------------------------------------------------------------
h_us_quad <- input_add_out_bloc(h_us_quad, out_name = "OUTPUT1",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "DOMAIN",
					TIM_TYPE = "0.01 0.1 1.0 180. 1800. 3600. 7200. 32400. 61200.",
					DAT_TYPE = "TECPLOT")

h_us_quad <- input_add_out_bloc(h_us_quad, out_name = "OUTPUT2",
					PCS_TYPE = "RICHARDS_FLOW",
					NOD_VALUES = "PRESSURE1 SATURATION1",
					GEO_TYPE = "POLYLINE OUT",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "0.01 0.1 1.0 180. 1800. 3600. 7200. 32400. 61200.")

# pcs ----------------------------------------------------------------------
h_us_quad <- input_add_pcs_bloc(h_us_quad, pcs_name = "PROCESS1",
					PCS_TYPE = "RICHARDS_FLOW",
					NUM_TYPE = "NEW")

# rfd ----------------------------------------------------------------------
h_us_quad <- input_add_rfd_bloc(h_us_quad, rfd_name = "CURVES1",
                               mkey = "CURVES",
                               data = tibble::tibble(
                                   time = c(0.26315789,
                                            0.39473684,
                                            0.52631579,
                                            0.57894737,
                                            0.63157895,
                                            0.65789474,
                                            0.78947368,
                                            0.81578947,
                                            0.84210526,
                                            0.86842105,
                                            0.89473684,
                                            0.92105263,
                                            0.94894737,
                                            0.96052632,
                                            0.97368421,
                                            0.98684211,
                                            1.00000000),
                                   value = c(44627.5611,
                                             26399.6732,
                                             15616.8683,
                                             12658.7874,
                                             10261.0136,
                                             9238.24221,
                                             5464.93174,
                                             4920.21206,
                                             4429.78757,
                                             3988.24638,
                                             3590.71602,
                                             3232.80969,
                                             2892.29884,
                                             2462.21599,
                                             2050.45507,
                                             1707.55369,
                                             1421.99634))
)
h_us_quad <- input_add_rfd_bloc(h_us_quad, rfd_name = "CURVES2",
                               mkey = "CURVES",
                               data = tibble::tibble(
                                   time = c(0.26315789,
                                            0.39473684,
                                            0.52631579,
                                            0.57894737,
                                            0.63157895,
                                            0.65789474,
                                            0.78947368,
                                            0.81578947,
                                            0.84210526,
                                            0.86842105,
                                            0.89473684,
                                            0.92105263,
                                            0.94894737,
                                            0.96052632,
                                            0.97368421,
                                            0.98684211,
                                            1.00000000),
                                   value = c(4.4323E-05,
                                             0.00026547,
                                             0.00159003,
                                             0.00325358,
                                             0.00665757,
                                             0.00952343,
                                             0.05704014,
                                             0.08159396,
                                             0.11671736,
                                             0.16696017,
                                             0.23883078,
                                             0.34163922,
                                             0.49931406,
                                             0.58449912,
                                             0.69907308,
                                             0.8361059,
                                             1.0000000))
)

# st ----------------------------------------------------------------------
h_us_quad <- input_add_st_bloc(h_us_quad, st_name = "SOURCE_TERM1",
					PCS_TYPE = "RICHARDS_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0")

# tim ----------------------------------------------------------------------
h_us_quad <- input_add_tim_bloc(h_us_quad, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "RICHARDS_FLOW",
					TIME_STEPS = "100 10 1000 100",
					TIME_END = "61200.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(h_us_quad, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = h_us_quad, ogs_exe = "ogs_fem",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/H_us/Wet/h_us_quad/log")
