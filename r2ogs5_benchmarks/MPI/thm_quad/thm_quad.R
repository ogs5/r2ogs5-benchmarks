thm_quad <- create_ogs5(sim_name = "thm_quad",
					sim_path = "r2ogs5_benchmarks/tmp/MPI/thm_quad",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
thm_quad <- input_add_bc_bloc(thm_quad, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 0.0"
)
thm_quad <- input_add_bc_bloc(thm_quad, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE TOP_C",
					DIS_TYPE = "CONSTANT 373.0"
)
thm_quad <- input_add_bc_bloc(thm_quad, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
thm_quad <- input_add_bc_bloc(thm_quad, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE BOTTOM",
					DIS_TYPE = "CONSTANT 0.0"
)
thm_quad <- input_add_bc_bloc(thm_quad, bc_name = "BOUNDARY_CONDITION5",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_Y1",
					GEO_TYPE = "POLYLINE TOP",
					DIS_TYPE = "CONSTANT 1.0",
					TIM_TYPE = "CURVE 2"
)
# ddc ----------------------------------------------------------------------
thm_quad <- input_add_blocs_from_file(thm_quad,
                                      sim_basename = "thm_quad",
                                      filename = "thm_quad.ddc",
                                      file_dir = "ogs5_benchmarks/MPI/thm_quad")
# gli ----------------------------------------------------------------------
thm_quad <- input_add_blocs_from_file(thm_quad,
					sim_basename = "thm_quad",
					filename = "thm_quad.gli",
					file_dir = "ogs5_benchmarks/MPI/thm_quad")

# ic ----------------------------------------------------------------------
thm_quad <- input_add_ic_bloc(thm_quad, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e5"
)
thm_quad <- input_add_ic_bloc(thm_quad, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "HEAT_TRANSPORT",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 285.0"
)
# mfp ----------------------------------------------------------------------
thm_quad <- input_add_mfp_bloc(thm_quad, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					DENSITY = "1 1.000000e+003",
					VISCOSITY = "1 1.000000e-003",
					SPECIFIC_HEAT_CAPACITY = "1 1.01e+3",
					HEAT_CONDUCTIVITY = "1 0.026"
)
# mmp ----------------------------------------------------------------------
thm_quad <- input_add_mmp_bloc(thm_quad, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 2.000000e-001",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 2.550000e-13"
)
# msh ----------------------------------------------------------------------
thm_quad <- input_add_blocs_from_file(thm_quad,
					sim_basename = "thm_quad",
					filename = "thm_quad.msh",
					file_dir = "ogs5_benchmarks/MPI/thm_quad")

# msp ----------------------------------------------------------------------
thm_quad <- input_add_msp_bloc(thm_quad, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 1.350000e+003",
					THERMAL = "EXPANSION:\n 1.0e-5\n CAPACITY:\n 1 1.091000e+003\n CONDUCTIVITY:\n 1 0.4200000e+000",
					ELASTICITY = "POISSION: 3.0000e-001\n YOUNGS_MODULUS:\n 1 0.350000e+08",
					PLASTICITY = "DRUCKER-PRAGER 1.0e6 -1.0e+5 19.0 2.0 0.0"
)
# num ----------------------------------------------------------------------
thm_quad <- input_add_num_bloc(thm_quad, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					LINEAR_SOLVER = "2 0 1.e-008 1000 1.0 1 4",
					COUPLING_CONTROL = "LMAX 1.e-3"
)
thm_quad <- input_add_num_bloc(thm_quad, num_name = "NUMERICS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					LINEAR_SOLVER = "2 1 1.e-012 1000 0.5 1 4",
					COUPLING_CONTROL = "LMAX 1.e-3"
)
thm_quad <- input_add_num_bloc(thm_quad, num_name = "NUMERICS3",
					PCS_TYPE = "DEFORMATION",
					LINEAR_SOLVER = "2 5 1.e-12 5000 1.0 1 4",
					PLASTICITY_TOLERANCE = "1.e-12",
					COUPLING_CONTROL = "LMAX 1.e-3"
)
# out ----------------------------------------------------------------------
thm_quad <- input_add_out_bloc(thm_quad, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 TEMPERATURE1 DISPLACEMENT_X1 DISPLACEMENT_Y1 STRESS_XX STRESS_XY STRESS_YY STRESS_ZZ STRAIN_XX STRAIN_XY STRAIN_YY STRAIN_PLS",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
thm_quad <- input_add_pcs_bloc(thm_quad, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					NUM_TYPE = "NEW"
)
thm_quad <- input_add_pcs_bloc(thm_quad, pcs_name = "PROCESS2",
					PCS_TYPE = "HEAT_TRANSPORT",
					NUM_TYPE = "NEW"
)
thm_quad <- input_add_pcs_bloc(thm_quad, pcs_name = "PROCESS3",
					PCS_TYPE = "DEFORMATION"
)
# rfd ----------------------------------------------------------------------
thm_quad <- input_add_rfd_bloc(thm_quad, rfd_name = "CURVES3",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										120.,
										2400.),
								value = c(0.,
										1.,
										1.))
)
thm_quad <- input_add_rfd_bloc(thm_quad, rfd_name = "CURVES4",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										1200.0,
										2400.0,
										4800.0,
										9600.0),
								value = c(0.,
										-2.4e-2,
										-4.8e-2,
										-9.6e-2,
										-19.2e-2))
)
thm_quad <- input_add_rfd_bloc(thm_quad, rfd_name = "CURVES5",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.000000e+000,
										1200.0,
										2400.0,
										4800.0,
										9600.0),
								value = c(1.e5,
										1.e5,
										1.e5,
										1.e5,
										1.e5))
)
# st ----------------------------------------------------------------------
thm_quad <- input_add_st_bloc(thm_quad, st_name = "SOURCE_TERM1",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE LEFT",
					DIS_TYPE = "CONSTANT_NEUMANN 1.0",
					TIM_TYPE = "CURVE 3"
)
thm_quad <- input_add_st_bloc(thm_quad, st_name = "SOURCE_TERM2",
					PCS_TYPE = "DEFORMATION",
					PRIMARY_VARIABLE = "DISPLACEMENT_X1",
					GEO_TYPE = "POLYLINE RIGHT",
					DIS_TYPE = "CONSTANT_NEUMANN -1.0",
					TIM_TYPE = "CURVE 3"
)
# tim ----------------------------------------------------------------------
thm_quad <- input_add_tim_bloc(thm_quad, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "24 100.",
					TIME_END = "2400.",
					TIME_START = "0.0",
					TIME_CONTROL = ""
)
thm_quad <- input_add_tim_bloc(thm_quad, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "HEAT_TRANSPORT",
					TIME_STEPS = "24 100.",
					TIME_END = "2400.",
					TIME_START = "0.0",
					TIME_CONTROL = ""
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(thm_quad,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(thm_quad, ogs_exe = "ogs-mpi",
					run_path = NULL,
					log_output = TRUE,
					use_mpi = TRUE,
					number_of_cores = 4,
					log_path = "r2ogs5_benchmarks/tmp/MPI/thm_quad/log")
