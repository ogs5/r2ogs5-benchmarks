mcwt <- create_ogs5(sim_name = "mcwt",
					sim_path = "r2ogs5_benchmarks/tmp/MPI/McWhorter",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
mcwt <- input_add_bc_bloc(mcwt, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POLYLINE left",
					DIS_TYPE = "CONSTANT 5000.0"
)
mcwt <- input_add_bc_bloc(mcwt, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE2",
					GEO_TYPE = "POLYLINE left",
					DIS_TYPE = "CONSTANT 2.e5"
)
# ddc ----------------------------------------------------------------------
mcwt <- input_add_blocs_from_file(mcwt,
                                  sim_basename = "mcwt",
                                  filename = "mcwt.ddc",
                                  file_dir = "ogs5_benchmarks/MPI/McWhorter")
# gli ----------------------------------------------------------------------
mcwt <- input_add_blocs_from_file(mcwt,
                    sim_basename = "mcwt",
					filename = "mcwt.gli",
					file_dir = "ogs5_benchmarks/MPI/McWhorter")

# ic ----------------------------------------------------------------------
mcwt <- input_add_ic_bloc(mcwt, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 5.e4"
)
# mfp ----------------------------------------------------------------------
mcwt <- input_add_mfp_bloc(mcwt, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 0.001"
)
mcwt <- input_add_mfp_bloc(mcwt, FLUID_NAME = "FLUID_PROPERTIES2",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1000",
					VISCOSITY = "1 0.001"
)
# mmp ----------------------------------------------------------------------
mcwt <- input_add_mmp_bloc(mcwt, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					POROSITY = "1 0.3",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.0e-10",
					PERMEABILITY_SATURATION = "6 0. 1.0 2 66 0.0 1.0 2 1e-9",
					CAPILLARY_PRESSURE = "6 5000"
)
# msh ----------------------------------------------------------------------
mcwt <- input_add_blocs_from_file(mcwt,
					sim_basename = "mcwt",
					filename = "mcwt.msh",
					file_dir = "ogs5_benchmarks/MPI/McWhorter")

# num ----------------------------------------------------------------------
mcwt <- input_add_num_bloc(mcwt, num_name = "NUMERICS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					ELE_MASS_LUMPING = "1",
					ELE_UPWINDING = "0 1.0",
					LINEAR_SOLVER = "2 1 1.e-12 2000 1.0 100 4",
					NON_LINEAR_SOLVER = "PICARD 1e-5 50 1.0"
)
# out ----------------------------------------------------------------------
mcwt <- input_add_out_bloc(mcwt, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 PRESSURE2 PRESSURE_W SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_X2 VELOCITY_Y2",
					GEO_TYPE = "POLYLINE TOP",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "500 1000 2000 4000 7000 10000"
)
mcwt <- input_add_out_bloc(mcwt, out_name = "OUTPUT2",
					NOD_VALUES = "PRESSURE1 PRESSURE2 PRESSURE_W SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_X2 VELOCITY_Y2",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "500 1000 2000 4000 7000 10000"
)
mcwt <- input_add_out_bloc(mcwt, out_name = "OUTPUT3",
					NOD_VALUES = "PRESSURE1 PRESSURE2 PRESSURE_W SATURATION1 VELOCITY_X1 VELOCITY_Y1 VELOCITY_X2 VELOCITY_Y2",
					GEO_TYPE = "POINT POINT4",
					DAT_TYPE = "TECPLOT",
					TIM_TYPE = "STEPS 1"
)
# pcs ----------------------------------------------------------------------
mcwt <- input_add_pcs_bloc(mcwt, pcs_name = "PROCESS1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					NUM_TYPE = "NEW",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------
mcwt <- input_add_rfd_bloc(mcwt, rfd_name = "CURVES2",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0000,
										0.0250,
										0.0500,
										0.0750,
										0.1000,
										0.1250,
										0.1500,
										0.1750,
										0.2000,
										0.2250,
										0.2500,
										0.2750,
										0.3000,
										0.3250,
										0.3500,
										0.3750,
										0.4000,
										0.4250,
										0.4500,
										0.4750,
										0.5000,
										0.5250,
										0.5500,
										0.5750,
										0.6000,
										0.6250,
										0.6500,
										0.6750,
										0.7000,
										0.7250,
										0.7500,
										0.7750,
										0.8000,
										0.8250,
										0.8500,
										0.8750,
										0.9000,
										0.9250,
										0.9500,
										0.9750,
										1.0000),
								value = c(50990.20,
										36055.51,
										29439.20,
										25495.10,
										22803.51,
										20816.66,
										19272.48,
										18027.76,
										16996.73,
										16124.52,
										15374.12,
										14719.60,
										14142.14,
										13627.70,
										13165.61,
										12747.55,
										12366.94,
										12018.50,
										11697.95,
										11401.75,
										11126.97,
										10871.15,
										10632.19,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10408.33,
										10000.00,
										10000.00,
										10000.00,
										10000.00,
										10000.00,
										10000.00,
										10000.00,
										10000.00,
										10000.00))
)
# tim ----------------------------------------------------------------------
mcwt <- input_add_tim_bloc(mcwt, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "MULTI_PHASE_FLOW",
					TIME_CONTROL = "PI_AUTO_STEP_SIZE\n 1 1.0e-4 1.0e-10 10s",
					TIME_END = "1.e1",
					TIME_START = "0.0"
)
# write input files ----------------------------------------------------------------------
ogs5_write_inputfiles(mcwt,"all")
# execute ogs ----------------------------------------------------------------------
ogs5_run(mcwt, ogs_exe = "ogs-mpi",
					run_path = NULL,
					log_output = TRUE,
					use_mpi = TRUE,
					number_of_cores = 4,
					log_path = "r2ogs5_benchmarks/tmp/MPI/McWhorter/log")
