ogs5_obj <- create_ogs5(sim_name = "ogs5_obj",
					sim_path = "r2ogs5_benchmarks/tmp/TES/Convection_2D/",
					sim_id = 1L)

# gli ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "conv_2D_cart",
					filename = "conv_2D_cart.gli",
					file_dir = "ogs5_benchmarks/TES/Convection_2D/")

# ic ----------------------------------------------------------------------
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION1",
					PCS_TYPE = "TES",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 1.0e+05"
)
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION2",
					PCS_TYPE = "TES",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 293"
)
ogs5_obj <- input_add_ic_bloc(ogs5_obj, ic_name = "INITIAL_CONDITION3",
					PCS_TYPE = "TES",
					PRIMARY_VARIABLE = "CONCENTRATION1",
					GEO_TYPE = "DOMAIN",
					DIS_TYPE = "CONSTANT 0.0"
)
# mcp ----------------------------------------------------------------------
ogs5_obj <- input_add_mcp_bloc(ogs5_obj, NAME = "N2",
					TRANSPORT_PHASE = "0",
					FLUID_PHASE = "0",
					MOBILE = "1",
					DIFFUSION = "1 9.6500e-05",
					MOL_MASS = "0.028"
)
ogs5_obj <- input_add_mcp_bloc(ogs5_obj, NAME = "H2O",
					TRANSPORT_PHASE = "0",
					FLUID_PHASE = "0",
					MOBILE = "1",
					DIFFUSION = "1 9.6500e-05",
					MOL_MASS = "0.018"
)
# mfp ----------------------------------------------------------------------
ogs5_obj <- input_add_mfp_bloc(ogs5_obj, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "GAS",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1.0",
					VISCOSITY = "1 3e-5",
					SPECIFIC_HEAT_CAPACITY = "1 1000.0",
					HEAT_CONDUCTIVITY = "1 10.0",
					SPECIFIC_HEAT_SOURCE = "500.0"
)
# mmp ----------------------------------------------------------------------
ogs5_obj <- input_add_mmp_bloc(ogs5_obj, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "0.01",
					POROSITY = "1 0.8",
					TORTUOSITY = "1 1.000000e+000",
					MASS_DISPERSION = "1 0.1 0.01",
					PERMEABILITY_TENSOR = "ISOTROPIC 5.0e-12",
					HEAT_TRANSFER = "2 1 0",
					PARTICLE_DIAMETER = "1 0.00005",
					INTERPHASE_FRICTION = "SOLID"
)
# msh ----------------------------------------------------------------------
ogs5_obj <- input_add_blocs_from_file(ogs5_obj,
                    sim_basename = "conv_2D_cart",
					filename = "conv_2D_cart.msh",
					file_dir = "ogs5_benchmarks/TES/Convection_2D/")

# msp ----------------------------------------------------------------------
ogs5_obj <- input_add_msp_bloc(ogs5_obj, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 1000",
					THERMAL = c("EXPANSION:", "1e-5",
					            "CAPACITY:", "1 50",
					            "CONDUCTIVITY:", "1 0.2"),
					REACTIVE_SYSTEM = "INERT",
					SPECIFIC_HEAT_SOURCE = "10.0"
)
# num ----------------------------------------------------------------------
ogs5_obj <- input_add_num_bloc(ogs5_obj, num_name = "NUMERICS1",
                    PCS_TYPE = "TES",
                    LOCAL_PICARD1 = "200 1.0e-10",
                    LINEAR_SOLVER = "2 6 1.0e-015 1000 1.0 101 4",
                    NON_LINEAR_SOLVER = "PICARD 1.0e-05 200 1.0",
                    NON_LINEAR_UPDATE_VELOCITY = "1",
                    ELE_GAUSS_POINTS = "2",
                    OVERALL_COUPLING = "20 1.0e-03"

)
# out ----------------------------------------------------------------------
ogs5_obj <- input_add_out_bloc(ogs5_obj, out_name = "OUTPUT1",
					NOD_VALUES = c("PRESSURE1", "TEMPERATURE1",
					               "CONCENTRATION1"),
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "PVD",
					TIM_TYPE = "STEPS 1000"
)
# pcs ----------------------------------------------------------------------
ogs5_obj <- input_add_pcs_bloc(ogs5_obj, pcs_name = "PROCESS1",
					PCS_TYPE = "TES",
					# TEMPERATURE_UNIT = ! is not a valid skey !"KELVIN",
					ELEMENT_MATRIX_OUTPUT = "0"
)
# rfd ----------------------------------------------------------------------
ogs5_obj <- input_add_rfd_bloc(ogs5_obj, rfd_name = "CURVES1",
					mkey = "CURVES",
					data = tibble::tibble(
								time = c(0.0,
										5.0,
										100000),
								value = c(0.0,
										1.0,
										1.0))
)
# st ----------------------------------------------------------------------
ogs5_obj <- input_add_st_bloc(ogs5_obj, st_name = "SOURCE_TERM1",
					PCS_TYPE = "TES",
					PRIMARY_VARIABLE = "TEMPERATURE1",
					GEO_TYPE = "POLYLINE WALL",
					DIS_TYPE = "TRANSFER_SURROUNDING 1.0 293.0"
)
# tim ----------------------------------------------------------------------
ogs5_obj <- input_add_tim_bloc(ogs5_obj, tim_name = "TIM_STEPPING1",
                    PCS_TYPE = "TES",
                    TIME_START = "0",
                    TIME_END = "5000",
                    TIME_STEPS = c("2000 0.02",
                                   "2000, 0.05",
                                   "1000 0.1",
                                   "3000 0.2",
                                   "1000 0.5",
                                   "2000 1.0",
                                   "2000 2.0")
)

ogs5_write_inputfiles(ogs5_obj, "all")
ogs5_run(ogs5_obj, "ogs_fem")
