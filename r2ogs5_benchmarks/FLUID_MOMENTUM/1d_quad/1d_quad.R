# Benchmark: fm1d_quad

# define ogs5 obj ---------------------------------------------------------
fm1d_quad <- create_ogs5(sim_name = "fm1d_quad",
					sim_path = "r2ogs5_benchmarks/tmp/FLUID_MOMENTUM/1d_quad",
					sim_id = 1L)

# bc ----------------------------------------------------------------------
fm1d_quad <- input_add_bc_bloc(fm1d_quad, bc_name = "BOUNDARY_CONDITION1",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT0",
					DIS_TYPE = "CONSTANT 9810")

fm1d_quad <- input_add_bc_bloc(fm1d_quad, bc_name = "BOUNDARY_CONDITION2",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT2",
					DIS_TYPE = "CONSTANT 9810")

fm1d_quad <- input_add_bc_bloc(fm1d_quad, bc_name = "BOUNDARY_CONDITION3",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT1",
					DIS_TYPE = "CONSTANT 0.0")

fm1d_quad <- input_add_bc_bloc(fm1d_quad, bc_name = "BOUNDARY_CONDITION4",
					PCS_TYPE = "LIQUID_FLOW",
					PRIMARY_VARIABLE = "PRESSURE1",
					GEO_TYPE = "POINT POINT3",
					DIS_TYPE = "CONSTANT 0.0")

# gli ----------------------------------------------------------------------
fm1d_quad <- input_add_blocs_from_file(fm1d_quad,
					filename = "1d_quad.gli",
					file_dir = "ogs5_benchmarks/FLUID_MOMENTUM/1d_quad")

# mfp ----------------------------------------------------------------------
fm1d_quad <- input_add_mfp_bloc(fm1d_quad, FLUID_NAME = "FLUID_PROPERTIES1",
					FLUID_TYPE = "LIQUID",
					# PCS_TYPE = ! is not a valid skey !"PRESSURE1",
					DENSITY = "1 1000.0",
					VISCOSITY = "1 1e-3",
					# HEAT_CAPACITY = ! is not a valid skey !"1 0.0",
					HEAT_CONDUCTIVITY = "1 0.0")

# mmp ----------------------------------------------------------------------
fm1d_quad <- input_add_mmp_bloc(fm1d_quad, NAME = "MEDIUM_PROPERTIES1",
					GEOMETRY_DIMENSION = "2",
					GEOMETRY_AREA = "1.000000e+000",
					POROSITY = "1 1.000",
					TORTUOSITY = "1 1.000000e+000",
					PERMEABILITY_TENSOR = "ISOTROPIC 1.000000e-12",
					MASS_DISPERSION = "1 0.25 0.5",
					# DENSITY = ! is not a valid skey !"1 2000.0"
					)

# msh ----------------------------------------------------------------------
fm1d_quad <- input_add_blocs_from_file(fm1d_quad,
					filename = "1d_quad.msh",
					file_dir = "ogs5_benchmarks/FLUID_MOMENTUM/1d_quad")

# msp ----------------------------------------------------------------------
fm1d_quad <- input_add_msp_bloc(fm1d_quad, NAME = "SOLID_PROPERTIES1",
					DENSITY = "1 2.00000e+003")

# num ----------------------------------------------------------------------
fm1d_quad <- input_add_num_bloc(fm1d_quad, num_name = "NUMERICS1",
					PCS_TYPE = "LIQUID_FLOW",
					ELE_GAUSS_POINTS = "2",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2")

fm1d_quad <- input_add_num_bloc(fm1d_quad, num_name = "NUMERICS2",
					PCS_TYPE = "FLUID_MOMENTUM",
					ELE_GAUSS_POINTS = "3",
					LINEAR_SOLVER = "2 6 1.e-014 1000 1.0 1 2")

# out ----------------------------------------------------------------------
fm1d_quad <- input_add_out_bloc(fm1d_quad, out_name = "OUTPUT1",
					NOD_VALUES = "PRESSURE1 VELOCITY1_X VELOCITY1_Y VELOCITY1_Z",
					GEO_TYPE = "DOMAIN",
					DAT_TYPE = "PVD",
					TIM_TYPE = "86400.0")

# pcs ----------------------------------------------------------------------
fm1d_quad <- input_add_pcs_bloc(fm1d_quad, pcs_name = "PROCESS1",
					PCS_TYPE = "LIQUID_FLOW",
					NUM_TYPE = "NEW")

fm1d_quad <- input_add_pcs_bloc(fm1d_quad, pcs_name = "PROCESS2",
					PCS_TYPE = "FLUID_MOMENTUM")

# tim ----------------------------------------------------------------------
fm1d_quad <- input_add_tim_bloc(fm1d_quad, tim_name = "TIME_STEPPING1",
					PCS_TYPE = "LIQUID_FLOW",
					TIME_STEPS = "1 86400",
					TIME_END = "86400.0",
					TIME_START = "0.0")

fm1d_quad <- input_add_tim_bloc(fm1d_quad, tim_name = "TIME_STEPPING2",
					PCS_TYPE = "FLUID_MOMENTUM",
					TIME_STEPS = "1 86400",
					TIME_END = "86400.0",
					TIME_START = "0.0")

# write input files -------------------------------------------------------
ogs5_write_inputfiles(fm1d_quad, "all")

# run ogs5 simulation -----------------------------------------------------
ogs5_run(ogs5_obj = fm1d_quad, ogs_exe = "inst/ogs/ogs_5.8",
         run_path = NULL,
         log_output = TRUE,
         log_path = "r2ogs5_benchmarks/tmp/FLUID_MOMENTUM/1d_quad/log")
