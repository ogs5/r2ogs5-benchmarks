# r2ogs5 Benchmarks

This repository contains the bechmarks for the R-OpenGeoSys(v5) API [*r2ogs5*](https://gitlab.opengeosys.org/ogs5/r2ogs5).
The benchmarks are available as *r2ogs5* scripts showing you how to run the official [OpenGeoSys-v5 benchmarks](https://github.com/ufz/ogs5-benchmarks) with the *r2ogs5* API.
You need to install the [*r2ogs5*](https://gitlab.opengeosys.org/ogs5/r2ogs5) package first.
If you have any questions to the R scripts, please file an issue [here](https://gitlab.opengeosys.org/ogs5/r2ogs5-benchmarks/-/issues).
Please, post benchmark content related questions on our [Q&A page](https://discourse.opengeosys.org/).

**List of respected Benchmarks**
  
This table lists the respected benchmarks. 
The column *ogs5 executable* refers to the compilation option explained [here](https://github.com/ufz/ogs5/blob/master/CMakeLists.txt).

| bm path/name | ogs5 executable | comments |  
|:---|:---|:---|  
| Anisotropy/moleculardiffusion |  FEM |  |   
| Anisotropy/permeability |  FEM |  |  
| Books/BMB3/Vogel/hm2_1Dcolumn1/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/hm2_1Dcolumn2/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/hm2_2Dmandel/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/m1_1Dlozenge/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/m1_1Dlozenge/ogs5mpi |  MPI |  |  
| Books/BMB3/Vogel/m1_2Dload/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/m1_2Dload/ogs5mpi |  MPI |  |  
| Books/BMB3/Vogel/m1_3Dload/ogs5 |  FEM |  |  
| Books/BMB3/Vogel/m1_3Dload/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m1_3Dtorsionogs5 |  FEM |  |  
| Books/BMB3/Vogel/m1_3Dtorsion/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m1_3Dwaves/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m1_3Dwaves/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m1_3Dwings/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m1_3Dwings/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_1Dlozenge/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m2_1Dlozenge/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_1Dlozengebt/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m2_1Dlozengebt/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_2Dload/ogs5 |  FEM |   
| Books/BMB3/Vogel/m2_2Dload/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_2Dloadbt/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m2_2Dloadbt/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_3Dload/ogs5 |  FEM |  | 
| Books/BMB3/Vogel/m2_3Dload/ogs5mpi |  MPI |  |   
| Books/BMB3/Vogel/m2_3Dloadbt/ogs5 |  FEM |  |   
| Books/BMB3/Vogel/m2_3Dloadbt/ogs5 |  MPI |  |   
| Books/BMB3/Vogel/tm1_2Dbeam/ogs5 |  FEM |  do not work |   
| Books/BMB3/Vogel/tm1_2Dbeam/ogs5mpi |  MPI |  do not work |   
| Books/BMB3/Vogel/tm1_3Dorigin/ogs5 |  FEM |  do not work |   
| Books/BMB3/Vogel/tm1_3Dorigin/ogs5mpi |  MPI |  do not work |   
| Books/BMB3/Vogel/tm1_3Dsquare/ogs5 |  FEM |  do not work |   
| Books/BMB3/Vogel/tm1_3Dsquare/ogs5mpi |  MPI |  do not work |   
| C/1d_analyt |  FEM |  |   
| C/calcite_gems |  GEMS |  |   
| C/calcite_pqc |  PQC |  long runtime |   
| C/decay |  FEM |  |   
| C/diffusion/aniso |  FEM |  |   
| C/diffusion/Diff_HTO_test |  FEM |  |   
| C/Engesgaard/2Kin/slow_kin_pqc |  IPQC |  |   
| C/Engesgaard/2Kin/slow_kin_pqc |  FEM? |  fails (base exe) |   
| C/hetk+n+restart |  FEM |  |   
| C/IPhreeqcCoupling/isotope_fractionation |  IPQC |  |   
| C/NAPL-dissolution/1D_NAPL-diss_flow/hex |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_flow/lin |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_flow/pri |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_flow/quad |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_flow/tet |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_trans/hex |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_trans/lin |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_trans/quad |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_trans/tet |  FEM |  |   
| C/NAPL-dissolution/1D_NAPL-diss_trans/tri |  FEM |  |   
| C/sorption/Freundlich |  FEM |  |   
| C/sorption/Henry |  FEM |  |   
| C/sorption/Langmuir |  FEM |  |   
| C/sorption_decay |  FEM |  |    
| DENSITY-DEPENDENT_FLOW/Elder |  FEM |  |   
| DENSITY-DEPENDENT_FLOW/constrBC_PressAsHead_tri |  FEM |  |   
| DENSITY-DEPENDENT_FLOW/wholeBC_PressAsHead_tri |  FEM |  |   
| FLUID_MOMENTUM/1d_pyra |  FEM |  |   
| FLUID_MOMENTUM/1d_tet |  FEM |  |   
| FLUID_PROPERTIES/H2T_McWhorter_PwSnw |  FEM |  does not work |   
| FLUID_PROPERTIES/H_CO2-FLOW |  FEM | |   
| FLUID_PROPERTIES/HT_EOS |  FEM |  does not work |   
| GAS_FLOW/BHP | FEM | long runtime |
| GAS_FLOW/EoS/CO2 | FEM | |
| GAS_FLOW/EoS/H2O | FEM | |
| GAS_FLOW/gas_flow | FEM | |
| GAS_FLOW/Gravity | FEM | |
| GAS_FLOW/Leakage | FEM | long runtime |
| GAS_FLOW/nonisothermal_gas_flow | FEM | |
| GAS_FLOW/Plume | FEM | long runtime |  
| GAS_FLOW/Tracertest/AdvDiff | FEM |  |  
| GAS_FLOW/Tracertest/AdvDiffSorption | FEM |  |  
| GROUNDWATER_FLOW/q_hex | FEM |  |  
| GROUNDWATER_FLOW/q_quad | FEM |  |  
| GROUNDWATER_FLOW/riv1_hex | FEM | does not work |  
| GROUNDWATER_FLOW/riv1_quad | FEM | does not work |  
| GROUNDWATER_FLOW/riv2_hex | FEM | does not work |  
| GROUNDWATER_FLOW/Transient_flow | FEM |  |  
| GROUNDWATER_FLOW/uc_pris | FEM |  |  
| GROUNDWATER_FLOW/uc_quad | FEM |  |  
| GROUNDWATER_FLOW/uc_tri | FEM |  |  
| H/HetGWFlow/2D | FEM |  |  
| H/HetGWFlow/3D | FEM |  |  
| H/sat_1D | FEM |  |  
| H/sat_2D | FEM |  |  
| H/Theis/GWF_Theis_1-5D | FEM |  |  
| H/Theis/GWF_Theis_2-5D | FEM |  |  
| H/Theis/LF_Theis_1-5D | FEM | unknown porosity model |  
| H/Theis/LF_Theis_2-5D | FEM | unknown porosity model |  
| H/Theis_1D | FEM |  |  
| H/Theis_2D | FEM |  |  
| H2/BuckleyLeverett | FEM |  |  
| H2/HeatPipe | FEM | does not work |  
| H2/LabGasInjec | FEM |  |  
| H2/Liakopoulos/hex | FEM |  |
| H2/Liakopoulos/Line | FEM | |  
| H2/Liakopoulos/Quad | FEM |  |  
| H2/Liakopoulos/Tet | FEM |  |  
| H2/Liakopoulos/Tri | FEM |  |  
| H2/McWhorter | FEM |  |
| H2/McWhorter/1D | FEM |  |  
| HM/hm_cc_tri_s | FEM |  |  
| HM/hm_dyn_tri | FEM |  |  
| HM/hm_foot_tet | FEM | does not work |  
| HM/hm_foot_tri | FEM |  |  
| HM/hm_tri | FEM | not sure |  
| HM/hm_unsat | FEM |  |  
| HM/excavation/BoreholeExcavation | FEM | ? |  
| H_us/Darcy/unconf_WO_rch | FEM |  |  
| H_us/Darcy/unconf_W_rch | FEM |  |  
| H_us/Drainage | FEM |  |  
| H_us/Dual/dual_van | FEM |  |  
| H_us/Dual/dual_vl | FEM |  |  
| H_us/RSM | FEM |  |  
| H_us/Wet/h_us_line_celia | FEM |  |  
| H_us/Wet/h_us_line_Forsyth | FEM |  |  
| H_us/Wet/h_us_line_Warrick | FEM | long runtime |  
| H_us/Wet/h_us_quad | FEM |  |  
| H_us/Wet/h_us_tri_freebc | FEM |  |  
| H_us/Wet/Transient | FEM |  |  
| M/creep/m_crp_tri | FEM | long runtime, many warnings |  
| M/creep/m_triax_lubby2 | FEM |  |  
| M/creep/uc_creep01 | FEM | very long runtime |  
| M/elasticity/M_e_displacement_3Du | FEM |  |
| M/elasticity/M_e_stress_3Du | FEM |  |
| M/elasticity/m_e_transiso_3D | FEM |  |
| M/pressure/Sphere_elastic | FEM |  |  
| M/pressure/Tube | FEM |  |  
| M/m_brick_l | FEM |  |  
| M/m_cc_quad_s | FEM |  |  
| M/m_cc_tri_s | FEM |  |  
| M/m_e_transiso_2D | FEM |  |  
| M/m_mises | FEM |  |  
| M/m_sdc | FEM |  |  
| M/m_ssy_quad | FEM | non convergence of linear solver |  
| M/m_triax_lubby1 | FEM |  |  
| MPI/h_tri | MPI | 5 domains specified |  
| MPI/McWhorter | MPI |  |  
| MPI/m_tri | MPI |  |  
| MPI/THM | MPI |  |  
| MPI/thm_quad | MPI |  |  
| MPI/urach | MPI |  |  
| MULTIPHASE/KueperProblem-PcPnw | FEM |  |  
| NUMERICS/DISCRETE_FEATURES/InclinedFeature/H_incline_45r_line | FEM |  |  
| NUMERICS/DISCRETE_FEATURES/InclinedFeature/H_incline_45r_quad | FEM |  |  
| NUMERICS/DISCRETE_FEATURES/Lauwerier/ | FEM |  |  
| NUMERICS/FEM_FCT | FEM |  |  
| NUMERICS/SUPG/T_adv_diff_steady_SUPG_line | FEM |  |
| NUMERICS/SUPG/T_adv_diff_transient_SUPG_line | FEM |  |
| PETSc/ConcreteCrack | PETSC_GEMS |  |  
| PETSc/hm1_1Dbeam | PETSC_GEMS |  |  
| PETSc/hm_tri | PETSC_GEMS |  |  
| PETSc/h_tri | PETSC_GEMS |  |  
| PETSc/KueperProblem-PS | PETSC_GEMS |  |  
| PETSc/m1_3Dload | PETSC_GEMS |  |  
| PETSc/McWhorter | PETSC_GEMS |  |  
| PETSc/Richards | PETSC_GEMS |  |  
| PETSc/tm1_3Dorigin | PETSC_GEMS |  |  
| RWPT/2DGrains | FEM |  |  
| RWPT/3DGrain | FEM |  |  
| RWPT/Forchheimer | FEM |  |  
| RWPT/Harter | FEM |  |  
| RWPT/HomoCube | FEM |  |  
| T/1d_thermal_expansion | FEM |  |  
| T/2units2faults | FEM |  |  
| T/CopyValueBHE | FEM |  |  
| T/Lauwerier | FEM |  |  
| T/Ogata-Banks | FEM |  |  
| T/t3d | FEM |  |  
| T/TDiff | FEM |  |  
| T/TDiff-wall | FEM |  |  
| T/Viscosity | FEM |  |  
| T/HT_var_density_1D | FEM |  |  
| T/T_1D_axi | FEM |  |  
| T/t_tri | FEM |  |  
| T2HC/Convection_2D | FEM |  |  
| T2HC/Convection_2D_axi | FEM |  |  
| T2HC/friction | FEM |  |  
| T2HC/heat_of_reaction | FEM |  |  
| T2HC/heat_transfer | FEM |  |  
| TCR/Temperature_BacGrowth |  |  |  
| TCR/Temperature_Diff | FEM |  |  
| TCR/Temperature_Disp | FEM |  |  
| TES/Convection_2D | FEM |  |  
| TES/Convection_2D_axi | FEM |  |  
| TES/friction | FEM |  |  
| TES/heat_of_reaction | FEM |  |  
| TH2M/H2M_TEP | FEM |  |  
| TH2M/Newton | FEM |  |  
| TH2M/th2m_quad | FEM | does not work |  
| THM/deco_2015_b2s1 | FEM | does not work |  
| THM/thm_decov | FEM |  |  
