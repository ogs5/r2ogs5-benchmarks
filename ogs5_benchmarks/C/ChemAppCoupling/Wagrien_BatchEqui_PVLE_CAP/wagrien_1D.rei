#REACTION_INTERFACE
  $MOL_PER
   VOLUME
  $WATER_CONCENTRATION
   VARIABLE
    Al[3+]
    CO3[2-]
    Ca[2+]
    Cl[-]
    Fe[2+]
    Fe[3+]
    H[+]
    HCO3[-]
    HS[-]
    K[+]
    Mg[2+]
    Na[+]
    OH[-]
    SO4[2-]
  $WATER_SPECIES_NAME
   water_liquid
  $DISSOLVED_NEUTRAL_CO2_SPECIES_NAME
   CO2
  $PRESSURE
   CONSTANT 305.87
  $TEMPERATURE
   CONSTANT 367.15
  $RESIDUAL
   RECORD
  $P_VLE
   PCO2 CO2
#STOP
