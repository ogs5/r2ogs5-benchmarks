#SOURCE_TERM 
$DIS_TYPE
 CONSTANT_NEUMANN 6.944e-5 
$GEO_TYPE
 SURFACE UP 
$PCS_TYPE
 OVERLAND_FLOW 
$PRIMARY_VARIABLE
 HEAD 

#SOURCE_TERM 
$DIS_TYPE
 CRITICALDEPTH 1 1.e-3 
$GEO_TYPE
 POLYLINE EAST 
$PCS_TYPE
 OVERLAND_FLOW 
$PRIMARY_VARIABLE
 HEAD 

#SOURCE_TERM 
$DIS_TYPE
 GREEN_AMPT 1 .13 2.3e-5 6.944e-5 0.344 
$GEO_TYPE
 SURFACE UP 
$PCS_TYPE
 OVERLAND_FLOW 
$PRIMARY_VARIABLE
 HEAD 

#STOP 
